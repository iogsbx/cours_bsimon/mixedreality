<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width" initial-scale="1">
  <meta http-equiv="X-UA-Compatible" content="IE=Edge">
  <meta name="format-detection" content="telephone=no">
  <title>OSA | Field of view of limitations in see-through HMD using geometric waveguides</title>
  <link title="schema(DC)" rel="schema.dc" href="http://purl.org/dc/elements/1.1/" />
<meta name="twitter:card" content="summary">
<meta name="twitter:description" content="Geometric waveguides are being integrated into head-mounted display (HMD) systems, where having see-through capability in a compact, lightweight form factor is required. We developed methods for determining the field of view (FOV) of such waveguide HMD systems and have analytically derived the FOV for waveguides using planar and curved geometries. By using real ray-tracing methods, we are able to show how the geometry and index of refraction of the waveguide, as well as the properties of the coupling optics, impact the FOV. Use of this analysis allows one to determine the maximum theoretical FOV of a planar or curved waveguide-based system.">
<meta name="twitter:image" content="https://www.osapublishing.org/getThumbnail.cfm?uri=ao-55-22-5924&amp;o=y">
<meta name="twitter:site" content="@osapublishing">
<meta name="twitter:title" content="Field of view of limitations in see-through HMD using geometric waveguides">
<meta name="dc.creator" content="Edward DeHoog">
<meta name="dc.creator" content="Jason Holmstedt">
<meta name="dc.creator" content="Tin Aye">
<meta name="dc.date" content="2016-08-01">
<meta name="dc.description" content="Geometric waveguides are being integrated into head-mounted display (HMD) systems, where having see-through capability in a compact, lightweight form factor is required. We developed methods for determining the field of view (FOV) of such waveguide HMD systems and have analytically derived the FOV for waveguides using planar and curved geometries. By using real ray-tracing methods, we are able to show how the geometry and index of refraction of the waveguide, as well as the properties of the coupling optics, impact the FOV. Use of this analysis allows one to determine the maximum theoretical FOV of a planar or curved waveguide-based system.">
<meta name="dc.identifier" content="doi:10.1364/AO.55.005924">
<meta name="dc.language" content="EN">
<meta property="og:description" content="Geometric waveguides are being integrated into head-mounted display (HMD) systems, where having see-through capability in a compact, lightweight form factor is required. We developed methods for determining the field of view (FOV) of such waveguide HMD systems and have analytically derived the FOV for waveguides using planar and curved geometries. By using real ray-tracing methods, we are able to show how the geometry and index of refraction of the waveguide, as well as the properties of the coupling optics, impact the FOV. Use of this analysis allows one to determine the maximum theoretical FOV of a planar or curved waveguide-based system.">
<meta property="og:image" content="https://www.osapublishing.org/getThumbnail.cfm?uri=ao-55-22-5924&amp;size=200&amp;o=y">
<meta property="og:sitename" content="OSA Publishing">
<meta property="og:title" content="Field of view of limitations in see-through HMD using geometric waveguides">
<meta property="og:type" content="Article">
<meta property="og:url" content="https://www.osapublishing.org/ao/abstract.cfm?uri=ao-55-22-5924">
<meta name="dc.publisher" content="Optical Society of America">
<meta name="dc.rights" content="&amp;#169; 2016 Optical Society of America">
<meta name="dc.source" content="Applied Optics, Vol. 55, Issue 22, pp. 5924-5930">
<meta name="dc.subject" content="Diffraction efficiency">
<meta name="dc.subject" content="Diffraction gratings">
<meta name="dc.subject" content="Optical systems">
<meta name="dc.subject" content="Planar waveguides">
<meta name="dc.subject" content="Refractive index">
<meta name="dc.subject" content="Waveguide gratings">
<meta name="dc.title" content="Field of view of limitations in see-through HMD using geometric waveguides">
<meta name="citation_abstract_html_url" content="https://www.osapublishing.org/abstract.cfm?uri=ao-55-22-5924">
<meta name="citation_author" content="Edward DeHoog">
<meta name="citation_author_institution" content="Optical Engineering and Analysis LLC, 1030 Loma Ave., Unit 102, Long Beach, California 90804, USA">
<meta name="citation_author" content="Jason Holmstedt">
<meta name="citation_author_institution" content="Physical Optics Corporation, 1845 W. 205th St., Torrance, California 90501, USA">
<meta name="citation_author" content="Tin Aye">
<meta name="citation_author_institution" content="Physical Optics Corporation, 1845 W. 205th St., Torrance, California 90501, USA">
<meta name="citation_doi" content="10.1364/AO.55.005924">
<meta name="citation_firstpage" content="5924">
<meta name="citation_fulltext_html_url" content="https://www.osapublishing.org/viewmedia.cfm?uri=ao-55-22-5924&amp;seq=0&amp;html=true">
<meta name="citation_issn" content="2155-3165">
<meta name="citation_issue" content="22">
<meta name="citation_journal_abbrev" content="Appl. Opt., AO">
<meta name="citation_journal_title" content="Applied Optics">
<meta name="citation_keywords" content="Diffraction efficiency;Diffraction gratings;Optical systems;Planar waveguides;Refractive index;Waveguide gratings;">
<meta name="citation_language" content="EN">
<meta name="citation_lastpage" content="5930">
<meta name="citation_online_date" content="2016/07/25">
<meta name="citation_pdf_url" content="https://www.osapublishing.org/viewmedia.cfm?uri=ao-55-22-5924&amp;seq=0">
<meta name="citation_publication_date" content="2016/08/01">
<meta name="citation_publisher" content="Optical Society of America">
<meta name="citation_title" content="Field of view of limitations in see-through HMD using geometric waveguides">
<meta name="citation_volume" content="55">


  <link rel="stylesheet" href="/css/token-input-facebook.css">
  <link rel="stylesheet" href="/css/app.css?v=05282019">
  <!--<link rel="stylesheet" href="/css/search.css">  Second style redefines first in app.css -->
  <link rel="stylesheet" href="/css/journal-colors.css">
  <link rel="stylesheet" href="/css/tools.css?v=01012018">
  <link rel="stylesheet" href="/css/jqueryui/jquery-ui.css">
  <link rel="stylesheet" href="/css/jqueryui/jquery-ui-1.10.3.custom.min.css">
<!--  <link rel="stylesheet" href="/css/prettyphoto/css/prettyPhoto.css">-->
  
  <!--[if lt IE 9]>
  <script type="text/javascript" src="/js/vendor/html5shiv.min.js"></script>
  <script type="text/javascript" src="/js/vendor/respond.min.js"></script>
  <![endif]-->
  
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&amp;subset=latin,greek-ext' rel='stylesheet' type='text/css'>
  
  <script type="text/javascript" src="/js/vendor/custom.modernizr.js"></script>
  <script type="text/javascript" src="https://scholar.google.com/scholar_js/casa.js" async></script> <script type="text/javascript" src="/js/Chart.min.js"></script>
</head>
<body class="single-article journal-ao">
<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
	<symbol id="right-arrow" viewBox="0 0 7.7 8.89">
	<defs>
		<style>
			.rarrow {
				stroke-miterlimit: 10;
			}
		</style>
	</defs>
		<title>Expand this Topic</title>
		<desc>clickable element to expand a topic</desc>
		<polygon class="rarrow" points="0.5 0.87 0.5 8.03 6.7 4.45 0.5 0.87" />
	</symbol>
</svg>

	<div class="site">
		
<nav class="primary navbar navbar-inverse hidden-xs">
  <div class="container">

    <div class="secondary-branding navbar-header">
      <a href="http://www.osa.org" class="secondary-brand navbar-brand"><img src="/images/OSA-sm-logo.png" alt="OSA | The Optical Society" /></a>
    </div>

    <div class="user-acct navbar-right">

      <ul class="manage-user-acct nav navbar-nav">
		<li class="nav-link"><a href="#" data-toggle="modal" data-target="#userLogin" id="loginModal">Login or Create Account</a></li>
		
      </ul>
    </div>

    
    

      <noscript>
      <div id="cookiePopup" class="maintenance-alert container">
        <div class="alert alert-success alert-dismissible fade in osap-maintenance-alert">
          <p class="text-left">
            This website uses cookies to deliver some of our products and services as well as for analytics and to provide you a more personalized experience. Click <a href="/cookies.cfm">here</a> to learn more. By continuing to use this site, you agree to our use of cookies. We've also updated our Privacy Notice. Click <a href="/privacy.cfm">here</a> to see what's new.
          </p>
          <a class="btn btn-primary" href="/close_cookie.cfm?redir=true" role="button" data-dismiss="alert" aria-label="close" onclick="this.href='#'; $.get('/close_cookie.cfm');">Allow All Cookies</a>
        </div>
      </div>
      </noscript>

      <div id="cookiePopup" class="maintenance-alert container hidden">
        <div class="alert alert-success alert-dismissible fade in osap-maintenance-alert">
          <p class="text-left">
            This website uses cookies to deliver some of our products and services as well as for analytics and to provide you a more personalized experience. Click <a href="/cookies.cfm">here</a> to learn more. By continuing to use this site, you agree to our use of cookies. We've also updated our Privacy Notice. Click <a href="/privacy.cfm">here</a> to see what's new.
          </p>
          <a class="btn btn-primary" href="/close_cookie.cfm?redir=true" role="button" data-dismiss="alert" aria-label="close" onclick="this.href='#'; $.get('/close_cookie.cfm');">Allow All Cookies</a>
          <button id="cookiePopupClose" class="close" data-dismiss="alert" type="button" aria-label="close"><span aria-hidden="true">&times;</span></button>
        </div>
      </div>
    

  </div>
</nav>
<header class="masthead container">
  <div>
    <div role="navigation">
      <div class="mobile-header">

        <div class="primary-branding col-sm-4 col-xs-7">
          <a href="https://www.osapublishing.org" class="primary-brand"><img src="/images/OSA-Publishing-Logo-Large.png" alt="OSA Publishing" /></a>
        </div>

        <div class="mobile-nav-toggle visible-xs col-xs-5">
          <button type="button" class="mobile-nav-btn navbar-toggle" data-toggle="collapse" data-target="#osaGlobalNav">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <button type="button" class="mobile-search-btn navbar-toggle" data-toggle="collapse" data-target="#mobile-search">
            <span class="fi-magnifying-glass"></span>
          </button>
        </div>

      </div>

      <div class="collapse" id="mobile-search">
        <div class="header-search col-sm-9 col-xs-12">
          <div class="input-group form-control">
            <input id="topSearchTextMobile" type="text" class="form-control input-sm" placeholder="Search All Publications" aria-label="Search All Publications">
            <div class="input-group-btn">
              <button type="button" class="btn btn-default btn-sm search-options">Options <span class="caret"></span></button>
              <a id="topSearchMobile" class="btn btn-primary btn-sm"><span class="fi-magnifying-glass"></span></a>
            </div>
          </div>
        </div>
      </div>

      <div class="header-search hidden-xs">
        <div class="search-bar input-group main-search">
  <form name="nojs" action="/search_noscript.cfm" class="form-inline">
    <input name="q" id="topSearchText" type="text" class="search-text-input form-control input-sm header-search_inputField" placeholder="Search All Publications" aria-label="Search All Publications">
    <div class="input-group-btn header-search_btnGroup">
      <button data-toggle="collapse" data-target="#advanced-search-popup" type="button" class="btn btn-default btn-sm search-options">Options <span class="icon-filter"></span> <span class="caret"></span></button>
      <button id="topSearch" type="submit" class="btn btn-primary btn-sm btn-search">
        <label for="topSearch" class="icon-search icon-white"></label>
      </button>
      
    </div>
  </form>
</div>

<form id="advanced-search-popup" class="collapse advanced-search-popup">
  <div class="row avs-header">
    <div class="col-sm-9">
      <span class="icon-filter filter-icon-sm"></span> Search Options
    </div>
    <div class="col-sm-3 close-container">
      <div class="close-top"><a href="#" data-toggle="collapse" data-target="#advanced-search-popup">Close <strong class="fi-x"></strong></a></div>
    </div>
  </div>
  <!--<div id="formErrors" style="display:none" title="Field Error"><p></p></div>-->
  <div class="avs-content">
    <div class="avs_grayout"></div>
    <div class="row">
      <div class="col-sm-2 avs-label">Keywords</div>
      <div class="col-sm-8">
        <input name="" id="txtSearch" type="text" class="form-control" placeholder="Enter search terms here" aria-label="Enter search terms here" />
        <input id="chkOtrResource" name="selectionChk" type="checkbox" value="" />  <span class="avs-inline-label-left">Only if other resources available (images, video, datasets)</span>
      </div>
      <div class="col-sm-2">
        <div><input name="chkSearchIn" type="checkbox" id="chk-searchin-metadata" CHECKED /> <span class="avs-inline-label-left">Title and Abstract</span></div>
        <div><input name="chkSearchIn" type="checkbox" id="chk-searchin-all"/> <span class="avs-inline-label-left">All text</span></div>
      </div>
    </div>
    <div class="row space-top">
      <div class="col-sm-2 avs-label">Authors</div>
      <div class="col-sm-8">
        <input type="text" id="authorSearch" class="form-control" placeholder="Enter author names here" />
        <span class="small muted">&bull; Use these formats for best results: Smith or J Smith</span>
        <span class="small muted"><br>&bull; Use a comma to separate multiple people: J Smith, RL Jones, Macarthur</span>
      </div>
      <div class="col-sm-2">
        Any :&nbsp;<input id="chkAny" name="chkAnyAll" type="radio" value="any" checked="checked"/>
        All :&nbsp;<input id="chkAll" name="chkAnyAll" type="radio" value="all"/>
      </div>
    </div>
    <div class="row space-top">
      <div class="col-sm-12">
        <div class="avs-help avs-divider">
          <a class="avs-help-link" data-action='slide-toggle' data-target='#avsHint1' href="#" title="Help Link">?</a>
          <div id="avsHint1" class="avs-help-message">
            <strong>Tips for preparing a search:</strong>
            <ul>
              <li>Keep it simple - don't use too many different parameters.</li>
              <li>Separate search groups with parentheses and Booleans. Note the Boolean sign must be in upper-case.
                <ul>
                  <li>Example: (diode OR solid-state) AND laser [search contains "diode" or "solid-state" and laser]</li>
                  <li>Example: (photons AND downconversion) - pump [search contains both "photons" and "downconversion" but not "pump"]</li>
                </ul>
              </li>
              <li>Improve efficiency in your search by using wildcards.</li>
                <ul>
                  <li>Asterisk ( * ) -- Example: "elect*" retrieves documents containing "electron," "electronic," and "electricity"</li>
                  <li>Question mark (?) -- Example: "gr?y" retrieves documents containing "grey" or "gray"</li>
                </ul>
              <li>Use quotation marks " " around specific phrases where you want the entire phrase only.</li>
              <li>For best results, use the separate Authors field to search for author names.</li>
              <li>Author name searching:
                <ul>
                  <li>Use these formats for best results: Smith or J Smith</li>
                  <li>Use a comma to separate multiple people: J Smith, RL Jones, Macarthur</li>
                  <li>Note: Author names will be searched in the keywords field, also, but that may find papers where the person is mentioned, rather than papers they authored.</li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        Search in:
      </div>
    </div>

    <div class="row space-top">
      <div class="col-sm-2">
          <input name="selectionChk" id="chkJournals" type="checkbox" class="checkbox" CHECKED> <span class="avs-inline-label-left">Journals</span>
      </div>
      <div class="col-sm-7">
        <div class="input-collapse">
          <input id="advsJournalsSelector" type="text" class="form-control" placeholder="All journals (type names or look up list)">
          <div class="input-group-btn" name="journal">
            <button type="button" class="btn btn-default dropdown-toggle avs-modal-open" data-toggle="dropdown"><span class="caret"></span></button>
          </div>
        </div>
      </div>
      <div class="col-sm-3 avs-tiny">
        <div class="avs-tiny_field">
          Vol. <input id="volSearch" type="text" class="form-control" placeholder="All" />
        </div>
        <div class="avs-tiny_field">
          Issue <input id="issueSearch" type="text" class="form-control" placeholder="All" />
        </div>
        <div class="avs-tiny_field">
          Page <input id="pageSearch" type="text" class="form-control" placeholder="All" />
        </div>
      </div>
    </div>
    <div class="row space-top-lg">
      <div class="col-sm-2">
        <input id="chkConferences" type="checkbox" class="checkbox" CHECKED> <span class="avs-inline-label-left">Proceedings</span>
      </div>
      <div class="col-sm-7">
        <div class="input-collapse">
          <input type="text" id="advsProceedingSelector" class="form-control" placeholder="All proceedings (type names or look up list)">
          <div class="input-group-btn" name="proceeding">
            <button type="button" class="btn btn-default dropdown-toggle avs-modal-open" data-toggle="dropdown"><span class="caret"></span></button>
          </div>
        </div>
      </div>
      <div class="col-sm-3 avs-tiny">
        <div class="avs-tiny_field">
          Year <input id="yearSearch" type="text" class="form-control" placeholder="All" />
        </div>
        <div class="avs-tiny_field avs-tiny_field--expanded">
          Paper # <input id="sessionPagerSearch" type="text" class="form-control" placeholder="All" />
        </div>
      </div>
    </div>

    <div class="row space-top">
      <div class="col-md-12">
        <div class="avs-help avs-divider"></div>
      </div>
    </div>

    <div class="row space-top">
      <div class="col-sm-3">Publication years</div>
      <div class="col-sm-5">
        <div class="form-inline">
          <div class="form-group">
            <span class="avs-inline-label-right">From</span>
            <input id="startYear" type="text" class="form-control year-inline" placeholder="1917" maxlength="4"/>
            <span class="avs-inline-label-left avs-inline-label-right">To</span>
            <input id="endYear" type="text" class="form-control year-inline" placeholder="2019" maxlength="4"/>
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div><small class="muted">Enter only one date to search</small></div>
        <div><small class="muted">After ("From") or Before ("To")</small></div>
      </div>
    </div>

    <div class="row space-top">
      <div class="col-md-12">
        <div class="avs-help avs-divider"></div>
      </div>
    </div>

    <div class="avs_topics">
      <div class="row space-top mbottom-10">
        <div class="col-sm-3">Optics &amp; Photonics Topics</div>
        <div class="col-sm-9 input-collapse avs_topics-search">
            <a id="browsetopicsbutton" href="#" class="btn btn-default avs_topics-browse-btn">Browse All Topics</a>
            <input type="text" id="advsTopicsSelector" class="form-control avs_topics-search-field" placeholder="Search All Topics"><br/>
        </div>
      </div>
      
      <div class="row space-top selectedtopicsadv mbottom-10">
        <div class="col-md-2"><small class="muted">Selected topics</small></div>
        <div class="col-md-10" id="advTopicsSelectedList"></div>
      </div>
      <div class="row selectedtopicsadv mbottom-10">
        <div class="col-md-2"></div>
        <div class="col-md-10 muted">
          <input id="chkAnyTopic" name="chkAnyAllTopic" type="radio" value="any" checked="checked"/> Find articles with <strong>any</strong> selected topics
          &nbsp;&nbsp;<input id="chkAllTopic" name="chkAnyAllTopic" type="radio" value="all"/> Find articles with <strong>all</strong> selected topics
        </div>
      </div>
      <div class="row hidden mbottom-20" id="pickTopicsBrowse">
        <div class="col-md-12 mbottom-20 ">
          <strong>Browse All Topics</strong><br>
          Click the <svg class="selection-list_arrow"><use xlink:href="#right-arrow" /></svg> to reveal subtopics. Use the checkbox to select a topic to filter your search.
        </div>
        <div id="advBrowseTopics" class="col-md-12">
        </div>
      </div>
      <div class="row space-top">
        <div class="col-md-12">
          <div class="avs-help avs-divider">
            <a class="avs-help-link" data-action='slide-toggle' data-target='#avsHint2' href="#" title="Help Link">?</a>
            <div id="avsHint2" class="avs-help-message">
              <strong>About Optics &amp; Photonics Topics</strong>
              <br>OSA Publishing developed the Optics and Photonics Topics to help organize its diverse content more accurately by topic area.  This topic browser contains over 2400 terms and is organized in a three-level hierarchy. <a href="/author/thesaurus/OSAThes_home.cfm" target="_blank">Read more.</a>
              <br><br>Topics can be refined further in the search results. The Topic facet will reveal the high-level topics associated with the articles returned in the search results.
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="row space-top">
      <div class="col-sm-2">Special Collections</div>
      <div class="col-sm-5 input-collapse">
        <input id="specialcollections_ee" name="specialcollections" type="checkbox" value="ee"/> <label class="avs-inline-label-left" for="specialcollections_ee">Energy Express</label><br/>
        <input id="specialcollections_el" name="specialcollections" type="checkbox" value="elnotes"/> <label class="avs-inline-label-left" for="specialcollections_el">Engineering and Laboratory Notes</label><br/>
      </div>
      <div class="col-sm-5 input-collapse">
        <input id="specialcollections_spt" name="specialcollections" type="checkbox" value="spotlight"/> <label class="avs-inline-label-left" for="specialcollections_spt">Spotlight on Optics</label><br/>
        <input id="specialcollections_tut" name="specialcollections" type="checkbox" value="tutorial"/> <label class="avs-inline-label-left" for="specialcollections_tut">Tutorials</label>
      </div>
    </div>

    <div class="row space-top">
      <div class="col-md-12">
        <div class="avs-help avs-divider"></div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-6"><a id="clearFilters" href="#" class="btn btn-default">Clear my choices above</a></div>
    <div class="col-sm-6"><a id="cmdSearch" href="#" class="btn btn-primary pull-right"><strong class="fi-magnifying-glass"></strong>&nbsp; Search</a></div>
    </div>

  </div>
  <input type="hidden" name="origsearchstr" id="origsearchstr" value=""/>
  <input type="hidden" name="facetsearchstr" id="facetsearchstr" value=""/>
  <input type="hidden" name="facetavoidstr" id="facetavoidstr" value=""/>
</form>

      </div>

      <div class="site-nav-container col-lg-8 col-md-12">
        
<nav class="primary-site-nav collapse navbar-collapse" id="osaGlobalNav">
	<ul class="nav navbar-nav navbar-main" id="first-nav">
		<li class="primary-site-nav__dropdown">
			<a href="/about.cfm" class="dropdown-toggle hidden-xs" id="select_journals">Journals <span class="caret"></span></a>
			<a href="" class="dropdown-toggle visible-xs" data-toggle="dropdown" data-target="select_journals--mobile__menu" id="select_journals--mobile">Journals <span class="caret"></span></a>
			
			<div class="visible-xs">
				<div id="select_journals--mobile__menu" class="dropdown-menu dropdown-one-col">
					<ul>
						<li><a href="/aop">Advances in Optics and Photonics</a></li>
						<li><a href="/ao">Applied Optics</a></li>
						<li><a href="/as">Applied Spectroscopy</a></li>
						<li><a href="/boe">Biomedical Optics Express</a></li>
						<li><a href="/col">Chinese Optics Letters</a></li>
						<li><a href="/copp">Current Optics and Photonics</a></li>
						<li><a href="/jlt">Journal of Lightwave Technology</a></li>
						<li><a href="/jnirs">Journal of Near Infrared Spectroscopy</a></li>
						<li><a href="/jot">Journal of Optical Technology</a></li>
						<li><a href="/jocn">Journal of Optical Communications and Networking</a></li>
						<li><a href="/josaa">Journal of the Optical Society of America A</a></li>
						<li><a href="/josab">Journal of the Optical Society of America B</a></li>
						<li><a href="/optica">Optica</a></li>
						<li><a href="/ome">Optical Materials Express</a></li>
						<li><a href="/oe">Optics Express</a></li>
						<li><a href="/ol">Optics Letters</a></li>
						<li><a href="/osac">OSA Continuum</a></li>
						<li><a href="/prj">Photonics Research</a></li>
						
						<li class="nav-link-heading">Legacy Journals</li>
						<li><a href="/jdt">Journal of Display Technology (2005-2016)</a></li>
						<li><a href="/josk">Journal of the Optical Society of Korea (1997-2016)</a></li>
						<li><a href="/jon">Journal of Optical Networking (2002-2009)</a></li>
						<li><a href="/josa">Journal of the Optical Society of America (1917-1983)</a></li>
						<li><a href="/on">Optics News (1975-1989)</a></li>
					</ul>
				</div>
			</div>
			<div class="dropdown-menu dropdown-two-col-lg dropdown-wide">
				<div class="row">
					<div class="col-sm-6">
						<ul>
							<li><a href="/aop">Advances in Optics and Photonics</a></li>
							<li><a href="/ao">Applied Optics</a></li>
							<li><a href="/as">Applied Spectroscopy</a></li>
							<li><a href="/boe">Biomedical Optics Express</a></li>
							<li><a href="/col">Chinese Optics Letters</a></li>
							<li><a href="/copp">Current Optics and Photonics</a></li>
							<li><a href="/jlt">Journal of Lightwave Technology</a></li>
							<li><a href="/jnirs">Journal of Near Infrared Spectroscopy</a></li>
							<li><a href="/jot">Journal of Optical Technology</a></li>
							<li><a href="/jocn">Journal of Optical Communications and Networking</a></li>
							<li><a href="/josaa">Journal of the Optical Society of America A</a></li>
							<li><a href="/josab">Journal of the Optical Society of America B</a></li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul>
							<li><a href="/optica">Optica</a></li>
							<li><a href="/ome">Optical Materials Express</a></li>
							<li><a href="http://www.osa-opn.org" target="_blank">Optics and Photonics News <span class="fa fa-external-link"></span></a></li>
							<li><a href="/oe">Optics Express</a></li>
							<li><a href="/ol">Optics Letters</a></li>
							<li><a href="/osac">OSA Continuum</a></li>
							<li><a href="/prj">Photonics Research</a></li>
							
							<li class="nav-link-heading">Legacy Journals</li>
							<li><a href="/jdt">Journal of Display Technology (2005-2016)</a></li>
							<li><a href="/josk">Journal of the Optical Society of Korea (1997-2016)</a></li>
							<li><a href="/jon">Journal of Optical Networking (2002-2009)</a></li>
							<li><a href="/josa">Journal of the Optical Society of America (1917-1983)</a></li>
							<li><a href="/on">Optics News (1975-1989)</a></li>
						</ul>
					</div>
				</div>
			</div>
		</li>
		<li class="primary-site-nav__dropdown">
			<a href="/conferences.cfm" class="dropdown-toggle" id="select_proceedings">Proceedings <span class="caret"></span></a>
			<div class="dropdown-menu dropdown-two-col-lg">
				<div class="row">
					<div class="col-sm-6">
						<ul>
							<li class="nav-link-heading">Find Proceedings</li>
							
							<li><a href="/conferences.cfm">By Year</a></li>
							<li><a href="/conferences.cfm?findby=conference">By Conference</a></li>
							<li class="nav-link-heading">Featured</li>
							<li><a href="/conference.cfm?meetingid=5">Optical Fiber Communication (OFC)</a></li>
							<li><a href="/conference.cfm?meetingid=124">Conference on Lasers and Electro-Optics (CLEO)</a></li>
							<li><a href="/conference.cfm?meetingid=56">Frontiers in Optics (FiO)</a></li>
						</ul>
					</div>
					
					<div class="col-sm-6">
						<ul>
							<li class="nav-link-heading">Newly Published</li>
							
							
								<li><a href="/conference.cfm?meetingid=104&yr=2019">Applied Industrial Optics: Spectroscopy, Imaging and Metrology</a></li>
							
								<li><a href="/conference.cfm?meetingid=40&yr=2019">Fourier Transform Spectroscopy</a></li>
							
								<li><a href="/conference.cfm?meetingid=44&yr=2019">Hyperspectral Imaging and Sounding of the Environment</a></li>
							
								<li><a href="/conference.cfm?meetingid=111&yr=2019">Optical Sensors</a></li>
							
								<li><a href="/conference.cfm?meetingid=194&yr=2019">Optics and Photonics for Sensing the Environment</a></li>
							
						</ul>
					</div>
				</div>
			</div>
		</li>
		<li class="primary-site-nav__dropdown hidden-xs">
			<a href="#global-nav" class="dropdown-toggle" id="select_other_resources">Other Resources <span class="caret"></span></a>
			<div class="dropdown-menu dropdown-two-col-lg">
				<div class="row">
					<div class="col-sm-6">
						<ul>
							<li class="nav-link-heading">Publications</li>
							<li><a href="/books/bookshelf/lasers.cfm"><em>Lasers</em> eBook</a></li>
							<li><a href="/books/bookshelf/osa-century-optics.cfm"><em>OSA Century of Optics</em></a></li>
							<li><a href="/books/bookshelf/opn-centennial-ebooklets.cfm">OPN Centennial eBooklets</a></li>
							<li><a href="/oida/reports.cfm">OIDA Reports</a></li>
							<li class="nav-link-heading">Products and Services</li>
							<li><a href="/osadigitalarchive.cfm">Digital Archive</a></li>
							<li><a href="/isp.cfm">Interactive Science Publishing (ISP)</a></li>
							<li><a href="http://imagebank.osa.org" target="_blank">Optics ImageBank <span class="fa fa-external-link"></span></a></li>
							<li><a href="/spotlight">Spotlight on Optics</a></li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul>
							<li class="nav-link-heading">Information For</li>
						    <li><a href="/author/author.cfm">Authors</a></li>
						    <li><a href="/submit/review/peer_review.cfm">Reviewers</a></li>
						    <li><a href="/library/">Librarians</a></li>
							<li class="nav-link-heading">Regional Sites</li>
							<li><a href="/china/">OSA Publishing China</a></li>
							
							<li class="nav-link-heading">Open Access Information</li>
							<li><a href="/submit/review/open-access-policy-statement.cfm">Open Access Statement and Policy</a></li>
							<li><a href="/library/license_v1.cfm">Terms for Journal Article Reuse</a></li>
						</ul>
					</div>
				</div>
			</div>
		</li>
	</ul>

  <ul class="nav navbar-nav navbar-right navbar-secondary" id="second-nav">
    <li class="primary-site-nav__dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="my_favorites">My Favorites <span class="caret"></span></a>
      <div class="dropdown-menu dropdown-one-col">
        <ul>
          
          <li><a href="/user">Go to My Account</a></li>
          
          <li><a href="#" data-toggle="modal" data-target="#userLogin">Login to access favorites</a></li>
          
        </ul>
      </div>
    </li>
    <li class="primary-site-nav__dropdown">
      <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="recent_pages">Recent Pages <span class="caret"></span></a>
      <div class="dropdown-menu dropdown-one-col">
        <ul class="recent-page-list">
        </ul>
      </div>
    </li>
  </ul>

	
  <ul class="hidden-sm hidden-md hidden-lg manage-user-acct--mobile">
<li class="nav-link"><a href="#" data-toggle="modal" data-target="#userLogin" id="loginModal">Login or Create Account</a></li>

  </ul>

</nav>

      </div>
    </div>
  </div>
</header> <div class="nav-bar-siblings container small"> <a href="https://www.osapublishing.org">OSA Publishing</a> > <a href="/ao/">Applied Optics</a>
	> <a href="/ao/browse.cfm?journal=9&strVol=55">Volume 55</a>
			> <a href="/ao/issue.cfm?volume=55&issue=22">Issue 22</a> > Page 5924 </div> 
<div class="container content" id="articleContainer">
	
<div class="col-md-12">
	<header class="article-header">
		
		<div class="row article-header__nav hidden-xs">
			
<div class="secondary-nav-container">
	<nav class="secondary-nav">
		<ul class="secondary-nav-list">
			<li class="secondary-nav-item hidden-md hidden-lg hidden-sm">
				<div class="mobile-journal-header">
		<a href="/ao/home.cfm"><h1>Applied Optics</h1>
		<p class="primary-editors">Ronald Driggers, Editor-in-Chief</p></a>
	</div>
			</li>
			<li class="secondary-nav-item hidden-xs active">
				<a href="/ao/home.cfm" class="secondary-nav-item_link">Journal Home</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/journal/ao/about.cfm">About</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/upcomingissue.cfm">Issues in Progress</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/issue.cfm">Current Issue</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/browse.cfm">All Issues</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/upcoming.cfm">Early Posting</a>
			</li> <li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/feature.cfm">Feature Issues</a>
			</li>
		</ul>
	</nav>
</div>

		</div>
		
		

		<div class="row">
			
			<div class="col-sm-10">
				<div class="row">
					
						<div class="article-thumbnail" id="thumbnailDiv">
							<img src="/getThumbnail.cfm?uri=ao-55-22-5924&s=n" alt="Article Cover" class="img-thumbnail hidden-xs article-thumbnail_img">
						</div>
					
					<div class="article-info">
					
					<h1 class="article-title">
						Field of view limitations in see-through HMDs using
					geometric waveguides 
					</h1>

					<p class="article-authors">
						Edward DeHoog, Jason Holmstedt, and Tin Aye
					</p>
					<div>
						
							<div class="btn-group article-affiliations-dropdown dropdown hidden-xs">
								<button type="button" class="btn btn-xs btn-default dropdown-toggle borderless" data-toggle="dropdown" id="author-affiliations-dropdown">
								Author Information  <span class="caret"></span>
								</button>
								
<div id="authorAffiliations" class="article-author-affiliations dropdown-menu">
	<h5>Author Affiliations</h5>
	 
		<p><strong> Edward DeHoog,<sup>1,</sup><sup>*</sup> Jason Holmstedt,<sup>2</sup> and Tin Aye<sup>2</sup> </strong></p>
		
		<p><em>
			<sup>1</sup>Optical Engineering and Analysis LLC, 1030
					Loma Ave., Unit 102, Long Beach, California 90804, USA
		</em></p>
		
		<p><em>
			<sup>2</sup>Physical Optics Corporation, 1845 W. 205th
					St., Torrance, California 90501, USA
		</em></p>
		
				<p>
					<sup>*</sup>Corresponding author: <a href="mailto:oea.dehoog@gmail.com">oea.dehoog@gmail.com</a>
				</p>
				
</div>

							</div>
						
						<div class="btn-group hidden-xs hidden-sm">
							<button type="button" class="btn btn-xs btn-default dropdown-toggle borderless" data-toggle="dropdown" id="search-by-author-dropdown">
								<i class="icon-search"></i>
									Find other works by these authors  <span class="caret"></span>
							</button>
							<ul class="dropdown-menu" role="menu" id="search-by-author">
								<li><a href="search.cfm?a=E  DeHoog">E  DeHoog</a></li> <li><a href="search.cfm?a=J  Holmstedt">J  Holmstedt</a></li> <li><a href="search.cfm?a=T  Aye">T  Aye</a></li> 
							</ul>
						</div>
						
					</div>
					 
				</div>
			
			</div>
		</div>
			
				<div class="col-md-2 visible-md visible-lg">
					<a href="/ao/"><img src="/images/osa-ao.jpg" alt="Applied Optics" class="pull-right img-responsive"></a>
				</div>
			
		</div>
	</header>
</div>

<div class="row">
	<div class="article-parents col-md-12">
	
		<div class="row">
	
			<ul class="small list-inline col-md-12 article-journal-name">
				<li><strong>Applied Optics</strong></li>
				<li>Vol. 55,</li>
				<li><a href="/ao/issue.cfm?volume=55&issue=22">Issue 22</a>,</li>
				<li>pp. 5924-5930</li>
				<li>(2016)</li>
				<li class="article-doi"><span class="separator-disc">&bull;</span><a href="https://doi.org/10.1364/AO.55.005924">https://doi.org/10.1364/AO.55.005924</a></li>
			</ul>
	
		</div>
	
	</div>
</div>


	<div class="row">
		<!-- Aux Sidebar -->
		<div class="col-sm-3 col-sm-push-9 article-secondary">
				<div class="article-tools">
  <ul class="tool-list-one">
    <li><a href="#" onclick="popUpWindow('submitEmail.cfm?id=347995&amp;URI=ao-55-22-5924',550,490); return false;"><i class="icon-email"></i> <span class="hidden-xs">Email</span></a></li>

    <li class="dropdown js-article-dropdown">
      <a class="dropdown-toggle js-article-dropdown-toggle"><i class="icon-share"></i> <span class="hidden-xs">Share <span class="caret"></span></span></a>
      
      <ul class="dropdown-menu list-unstyled">
		

        <li><a href="http://www.facebook.com/sharer.php?u=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924" class="sharefacebook" target="_blank"><img src="/images/facebook-icon.png" title="Facebook" alt="Share with Facebook" class="share-icon">Share with Facebook</a></li>
        <li><a href="http://twitter.com/home?status=Reading this AO article %23OSA_AO https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924" class="sharetwitter" target="_blank"><img src="/images/twitter-icon.png" title="Twitter" alt="Tweet This" class="share-icon">Tweet This</a></li>
        <li><a href="http://www.reddit.com/submit?url=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924" class="sharereddit" target="_blank"><img src="/images/reddit-icon.gif" title="reddit" alt="reddit" class="share-icon">Post on reddit</a></li>
        <li><a href="https://www.linkedin.com/cws/share?url=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924" class="sharelinkedin" target="_blank"><img src="/images/linkedin-icon.png" title="linkedin" alt="linkedin" class="share-icon">Share with LinkedIn</a></li>
        <li><a href="http://www.citeulike.org/posturl?url=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924&title=Field%20of%20view%20of%20limitations%20in%20see%2Dthrough%20HMD%20using%20geometric%20waveguides" class="shareciteulike" target="_blank"><img src="/images/citeulike-icon.png" Alt="Add to CiteULike" title="CiteULike" class="share-icon">Add to CiteULike</a></li>
        <li><a href="http://www.mendeley.com/import/?url=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924" class="sharemendeley" target="_blank"><img src="/images/mendeley-icon.png" title="Mendeley" alt="Mendeley" class="share-icon">Add to Mendeley</a></li>
        <li><a href="http://www.bibsonomy.org/ShowBookmarkEntry?c=b&jump=yes&url=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Furi%3Dao%2D55%2D22%2D5924" class="sharebibsonomy" target="_blank"><img src="/images/bibsonomy-icon.png" alt="Add to BibSonomy" title="BibSonomy" class="share-icon">Add to BibSonomy</a></li>
      </ul>
    </li>
  </ul>

  <ul class="tool-list-two">
    <li class="get-citation dropdown js-article-dropdown">

      <a id="getCitation__toggle" class="dropdown-toggle js-article-dropdown-toggle">
        <i class="icon-get-citation"></i>
        <span class="hidden-xs">Get Citation</span> <span class="caret"></span>
      </a>

      <div id="getCitation" class="get-citation-content dropdown-menu">
        <strong>Copy Citation Text</strong>
        <div class="citation-text">
          Edward DeHoog, Jason Holmstedt, and Tin Aye, "Field of view of limitations in see-through HMD using geometric waveguides," Appl. Opt.  <b>55</b>, 5924-5930 (2016)
		
        </div>

        <form name="articlesForm" id="articlesForm" method="post" action="" style="display: inline">
        <input type="hidden" name="articles" value="347995">
        <input type="hidden" name="ArticleAction" value=""></form>
        <strong>Export Citation</strong>
        <ul class="list-unstyled">
          <li><a href="#" onclick="checkboxActions('export_bibtex'); return false;">BibTex</a></li>
          <li><a href="#" onclick="checkboxActions('export_endnote'); return false;">Endnote (RIS)</a></li>
          <li><a href="#" onclick="checkboxActions('export_html'); return false;">HTML</a></li>
          <li><a href="#" onclick="checkboxActions('export_txt'); return false;">Plain Text</a></li>
        </ul>
      </div>
    </li>
    <li class="pdf-download"><a href="viewmedia.cfm?uri=ao-55-22-5924&seq=0" target="_blank"><i class="icon-pdf"></i> <span class="hidden-xs">Get PDF (913 KB)</span></a></li> <li data-extended-text=" for article"><a href="#" data-toggle="modal" data-target="#userLogin"><i class="icon-citation-alert"></i> <span class="hidden-xs">Set citation alerts</span></a></li>
    <li data-extended-text=" to My Favorites"><a href="/user/favorites_add_article.cfm?articles=347995" target="_blank"><i class="icon-save-to-favorites"></i> <span class="hidden-xs">Save article</span></a></li>
    
  </ul>
</div>




			<div class="sidebar-nav hidden-xs">
				

				<ul class="right-nav">
					<li class="sub-list">
						<input type="checkbox" class="osap-accordion__toggle" id="toggle-relatedTopics" autocomplete="off">
						<label for="toggle-relatedTopics" class="osap-accordion__label right-nav__label">Related Topics<i class="fa osap-accordion__chevron"></i></label>
						<div id="relatedTopics" class="osap-accordion__content">
							
							<em>Table of Contents Category</em>
							<ul class="list-unstyled">
								<li>Geometrical Optics</li>
							</ul>
							<br>
							
								<div id="topicsHelp" class="get-citation dropdown">
									<em>Optics &amp; Photonics Topics</em> <div class="advanced-search-popup advanced-search-popup--article"><a id="topicsHelp__toggle" class="topicsHelp__toggle" title="Help Link">?</a></div>
									<div class="get-citation-content dropdown-menu">
										<span class="small">The topics in this list come from the <a href="/author/thesaurus/OSAThes_home.cfm" target="_blank">OSA Optics and Photonics Topics</a> applied to this article.</span>
									</div>
								</div>

								<ul class="list-unstyled">
								<li><a class="js-relatedTopicLink" href="search.cfm?t=Physical optics|Wave optics|Diffraction efficiency">Diffraction efficiency</a></li> <li><a class="js-relatedTopicLink" href="search.cfm?t=Equipment|Optical components|Diffraction gratings">Diffraction gratings</a></li> <li><a class="js-relatedTopicLink" href="search.cfm?t=Optical systems">Optical systems</a></li> <li><a class="js-relatedTopicLink" href="search.cfm?t=Equipment|Optical devices|Planar waveguides">Planar waveguides</a></li> <li><a class="js-relatedTopicLink" href="search.cfm?t=Optical design|Geometric optics|Refractive index">Refractive index</a></li> <li><a class="js-relatedTopicLink" href="search.cfm?t=Equipment|Optical components|Waveguide gratings">Waveguide gratings</a></li> 
								</ul>
								<br>
								
								<em>Previously assigned OCIS codes</em>
								<ul class="list-unstyled">
								
									<li class="muted">Geometric optics  (080.0080)
										
									</li>
								
									<li class="muted">Geometric optical design (080.2740)
										
									</li>
								
									<li class="muted">Displays (120.2040)
										
									</li>
								
									<li class="muted">Heads-up displays (120.2820)
										
									</li>
								
									<li class="muted">Optical design of instruments (120.4570)
										
									</li>
								
									<li class="muted">Optical systems (120.4820)
										
									</li>
								
								</ul>
							
						</div>
					</li>
				</ul>

				
				<ul class="right-nav">
					<li class="sub-list">
							<input type="checkbox" class="osap-accordion__toggle" id="toggle-aboutArticle" checked autocomplete="off">
							<label for="toggle-aboutArticle" class="osap-accordion__label right-nav__label">About this Article<i class="fa osap-accordion__chevron"></i></label>

							<div id="aboutThisArticle" class="osap-accordion__content">
								
								<em>History</em>
								<ul class="list-unstyled small">
									<li>Original Manuscript: May 2, 2016</li> <li>Revised Manuscript:  June 29, 2016</li> <li>Manuscript Accepted: June 30, 2016</li> <li>Published:  July 25, 2016</li> 
								</ul>
								
							</div>
						
					</li>
				</ul>
				
			</div>

			<div id="referenceInfo" class="reference-info">
			<!-- Dynamic Reference Info -->
			</div>
		</div>

		<!-- Main Content -->
		<div class="col-sm-9 col-sm-pull-3">
			<div class="row">
				<!-- Article Navigation -->
				<div class="article-sidebar col-md-3">
					<div class="access-permissions accessible">
							<strong><i class="icon-accessible icon-blue"></i> Accessible</strong>
							<p class="muted">Full-text access provided by Universite de Bordeaux I Bibliotheque Service Commun de la Documentation</p>
						</div> <div id="articleNav" class="article-nav sub-page-nav">
	
		<ul class="sidebar-nav-list" data-action="change-section" data-target="articleBody">
			
				<li class="sidebar-nav-i"><a href="#Abstract" class="sidebar-nav-link">Abstract</a></li>
				<li class="sidebar-nav-i"><a href="viewmedia.cfm?uri=ao-55-22-5924&seq=0&html=true" class="sidebar-nav-link">Full Article</a></li> 
		</ul>
		<ul class="sidebar-nav-list navextras">
			<li class="sidebar-nav-i"><a id="figurestablink" href="#articleFigures" class="sidebar-nav-link" data-action="change-section" data-target="articleFigures">Figures (8)</a></li> <li class="sidebar-nav-i"><a href="#articleEquations" class="sidebar-nav-link" data-action="change-section" data-target="articleEquations">Equations (36)</a></li> 
		</ul>

		<ul class="sidebar-nav-list navextras">
			<li class="sidebar-nav-i"><a href="#articleReferences" class="sidebar-nav-link" data-action="change-section" data-target="articleReferences">References (9)</a></li>
			<li class="sidebar-nav-i"><a href="#articleCitations" class="sidebar-nav-link" data-action="change-section" data-target="articleCitations">Cited By<span id="numrefs"></span></a></li>
			<li class="sidebar-nav-i"><a href="#articleMetrics" class="sidebar-nav-link" data-action="change-section" data-target="articleMetrics" onclick="showMetrics();">Metrics</a></li>
		</ul>

		

		
		<ul class="sidebar-nav-list">
			<li class="sidebar-nav-i"><a href="#top" class="sidebar-nav__top-link">Back to Top</a></li>
			
			<li class="sidebar-nav-i">
				<a href="viewmedia.cfm?uri=ao-55-22-5924&seq=0" target="_blank"><i class="icon-pdf"></i>&nbsp;Get PDF</a>
			</li>
			
			</li>
		</ul>
	
</div> 
				</div>

				<!-- Article Body -->
				
				<div class="main-content col-md-9">
					<div id="articleBody" class="article-section page-section active">
						
						

						<h2 class="article-heading" id="Abstract">Abstract</h2>
						<p><p xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:xlink="http://www.w3.org/1999/xlink">Geometric waveguides are being integrated into head-mounted display
					(HMD) systems, where having see-through capability in a compact,
					lightweight form factor is required. We developed methods for
					determining the field of view (FOV) of such waveguide HMD systems and
					have analytically derived the FOV for waveguides using planar and
					curved geometries. By using real ray-tracing methods, we are able to
					show how the geometry and index of refraction of the waveguide, as
					well as the properties of the coupling optics, impact the FOV. Use of
					this analysis allows one to determine the maximum theoretical FOV of a
					planar or curved waveguide-based system.</p></p> 
									<p>© 2016 Optical Society of
					America</p>
								
								<a href="viewmedia.cfm?uri=ao-55-22-5924&seq=0&html=true" id="link-full">Full Article</a>&nbsp; |&nbsp;
							
								<a href="viewmedia.cfm?uri=ao-55-22-5924&seq=0" target="_blank" id="link-pdf">PDF Article</a>
							
							<div class="alert alert-gray" style="padding: 2px 6px; margin:16px 0px;">
							<h2 class="article-heading">Corrections</h2>
							
									<p>21 October 2016: A correction was made to the title.</p>
								
							</div>
							
						<hr style="margin-top:5em;">
						<strong>OSA Recommended Articles</strong>
							
								<div class="content-item media-twbs">
									<div class="media-twbs-left">
										<img class="media-object" src="/getThumbnail.cfm?uri=oe-24-5-4749&o=y" width="50" height="50" aria-hidden="true" role="presentation">
									</div>
									<div class="media-twbs-body">
										<a class="js-relatedArticleLink" href="/oe/abstract.cfm?uri=oe-24-5-4749">Optical design for a see-through head-mounted display with high visibility</a>
										<p>Kai-Wei Zhao and Jui-Wen Pan<br>
										<span class="muted">Opt. Express <strong>24</strong>(5) 4749-4760 (2016)</span></p>
									</div>
								</div>
								
								<div class="content-item media-twbs">
									<div class="media-twbs-left">
										<img class="media-object" src="/getThumbnail.cfm?uri=ao-56-4-901&o=y" width="50" height="50" aria-hidden="true" role="presentation">
									</div>
									<div class="media-twbs-body">
										<a class="js-relatedArticleLink" href="/ao/abstract.cfm?uri=ao-56-4-901">Alternate optical designs for head-mounted displays with a wide field of view</a>
										<p>Bo Chen and Alois M. Herkommer<br>
										<span class="muted">Appl. Opt. <strong>56</strong>(4) 901-906 (2017)</span></p>
									</div>
								</div>
								
								<div class="content-item media-twbs">
									<div class="media-twbs-left">
										<img class="media-object" src="/getThumbnail.cfm?uri=ao-54-28-E15&o=y" width="50" height="50" aria-hidden="true" role="presentation">
									</div>
									<div class="media-twbs-body">
										<a class="js-relatedArticleLink" href="/ao/abstract.cfm?uri=ao-54-28-E15">Design and assessment of a wide FOV and high-resolution optical tiled head-mounted display</a>
										<p>Weitao Song, Dewen Cheng, Zhaoyang Deng, Yue Liu, and Yongtian Wang<br>
										<span class="muted">Appl. Opt. <strong>54</strong>(28) E15-E22 (2015)</span></p>
									</div>
								</div>
								
								<div id="relatedmorelink" class="content-item media-twbs-body" style="text-align:center">
									<a href"#" onclick="$('#relatedmore').show(); $('#relatedmorelink').hide(); return false;">More Recommended Articles</a>
								</div>
								<div id="relatedmore" style="display:none">
								
								<div class="content-item media-twbs">
									<div class="media-twbs-left">
										<img class="media-object" src="/getThumbnail.cfm?uri=ao-56-31-8822&o=y" width="50" height="50" aria-hidden="true" role="presentation">
									</div>
									<div class="media-twbs-body">
										<a class="js-relatedArticleLink" href="/ao/abstract.cfm?uri=ao-56-31-8822">Flat metaform near-eye visor</a>
										<p>Chuchuan Hong, Shane Colburn, and Arka Majumdar<br>
										<span class="muted">Appl. Opt. <strong>56</strong>(31) 8822-8827 (2017)</span></p>
									</div>
								</div>
								
								<div class="content-item media-twbs">
									<div class="media-twbs-left">
										<img class="media-object" src="/getThumbnail.cfm?uri=ao-48-14-2655&o=y" width="50" height="50" aria-hidden="true" role="presentation">
									</div>
									<div class="media-twbs-body">
										<a class="js-relatedArticleLink" href="/ao/abstract.cfm?uri=ao-48-14-2655">Design of an optical see-through head-mounted display with a low f-number and large field of view using a freeform prism</a>
										<p>Dewen Cheng, Yongtian Wang, Hong Hua, and M. M. Talha<br>
										<span class="muted">Appl. Opt. <strong>48</strong>(14) 2655-2668 (2009)</span></p>
									</div>
								</div>
								
								</div>
								
					</div>
					
					<div id="articleReferences" class="article-section page-section small">
						

<h3 class="article-heading">References</h3>


	<ul class="inline-list sub-page-nav">
	  <li>View by:</li>
	  <li data-action="change-section" data-target="referenceById" class="subsection-link active">Article Order</li>
	  <li class="inline-list-divider">|</li>
	  <li data-action="change-section" data-target="referenceByYear" class="subsection-link">Year</li>
	  <li class="inline-list-divider">|</li>   <li data-action="change-section" data-target="referenceByAuthor" class="subsection-link">Author</li>
	  <li class="inline-list-divider">|</li>
	  <li data-action="change-section" data-target="referenceByPublication" class="subsection-link">Publication</li>
	</ul>

	<hr>

	
		<ol id="referenceById" class="sources-section page-section references-by-id active">
			
				<li> 
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	</li>
			
				<li> H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		</li>
			
				<li> B. C. Kress and  M. Shin, “Diffractive and holographic optics as optical combiners in head mounted displays,” presented at Ubicomp, Wearable Systems for Industrial Augmented Reality Applications, Zurich, Switzerland, 8–12 September2013. 
	<br>
	
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?&genre=article&stitle=presented%20at%20&atitle=Diffractive%20and%20holographic%20optics%20as%20optical%20combiners%20in%20head%20mounted%20displays">[360 Link]</a>
		</li>
			
				<li> 
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	</li>
			
				<li> 
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-41-7-1236">R. Shechter,  Y. Amitai,  and  A. A. Friesem, “Compact beam expander with linear gratings,” Appl. Opt. 41, 1236–1240 (2002).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.41.001236" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E41%2E001236&genre=article&stitle=Appl%2E%20Opt%2E&volume=41&issue=7&spage=1236&atitle=Compact%20beam%20expander%20with%20linear%20gratings">[360 Link]</a>
	</li>
			
				<li> A. Cameron, “The application of holographic optical waveguide technology to Q-sight family of helmet mounted displays,” Proc. SPIE 7326, 1–11 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.818581" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E818581&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=The%20application%20of%20holographic%20optical%20waveguide%20technology%20to%20Q%2Dsight%20family%20of%20helmet%20mounted%20displays">[360 Link]</a>
		</li>
			
				<li> R. Atac, “Applications of the scorpion color helmet-mounted cueing system,” Proc. SPIE 7688, 1–7 (2010). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.849287" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E849287&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=Applications%20of%20the%20scorpion%20color%20helmet%2Dmounted%20cueing%20system">[360 Link]</a>
		</li>
			
				<li> W. T. Welford, Aberrations of Optical Systems (Adam Hilger, 1986). 
	<br>
	</li>
			
				<li> E. L. Dereniak and  T. D. Dereniak, Geometrical and Trigonometric Optics (Cambridge University, 2008). 
	<br>
	</li>
			
		</ol>

		
		<div id="referenceByYear" class="sources-section page-section">
			
			
			<canvas id="referencesChart" width="600" height="280"></canvas>
			<script>
			var ctx = document.getElementById('referencesChart').getContext('2d');
			var myChart = new Chart(ctx, {
				type: 'bar',
				data: {
					labels: [2014,2013,2012,2011,2010,2009,2008,2007,2006,2005,2004,2003,2002],
					datasets: [{
						label: 'References',
						data: [1,0,0,0,1,2,0,0,1,0,0,0,1],
						backgroundColor: 'rgba(0, 0, 255, 0.6)',
						borderColor: 'rgba(54, 54, 54, 1)',
						borderWidth: 1
					}]
				},
				options: {
					legend: {display: false},
					scales: {
						xAxes: [{
							ticks: {autoSkip: true, maxTicksLimit: 20},
							scaleLabel: {display: true, labelString: 'Year', fontStyle: 'bold'}
						}],
						yAxes: [{
							ticks: {maxTicksLimit: 8, precision: 0},
							scaleLabel: {display: true, labelString: 'References', fontStyle: 'bold'}
						}]
					}
				}
			});
			</script>
			<br>
			
				<a name="refyear2014"></a><h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceYear2014">2014 (1)</h4>
				<div id="referenceYear2014" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
				<a name="refyear2010"></a><h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceYear2010">2010 (1)</h4>
				<div id="referenceYear2010" class="sources-container">
					<p class="source-block">
					R. Atac, “Applications of the scorpion color helmet-mounted cueing system,” Proc. SPIE 7688, 1–7 (2010). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.849287" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E849287&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=Applications%20of%20the%20scorpion%20color%20helmet%2Dmounted%20cueing%20system">[360 Link]</a>
		
					</p>
				</div>
			
				<a name="refyear2009"></a><h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceYear2009">2009 (2)</h4>
				<div id="referenceYear2009" class="sources-container">
					<p class="source-block">
					A. Cameron, “The application of holographic optical waveguide technology to Q-sight family of helmet mounted displays,” Proc. SPIE 7326, 1–11 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.818581" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E818581&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=The%20application%20of%20holographic%20optical%20waveguide%20technology%20to%20Q%2Dsight%20family%20of%20helmet%20mounted%20displays">[360 Link]</a>
		
					</p><p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
				<a name="refyear2006"></a><h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceYear2006">2006 (1)</h4>
				<div id="referenceYear2006" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p>
				</div>
			
				<a name="refyear2002"></a><h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceYear2002">2002 (1)</h4>
				<div id="referenceYear2002" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-41-7-1236">R. Shechter,  Y. Amitai,  and  A. A. Friesem, “Compact beam expander with linear gratings,” Appl. Opt. 41, 1236–1240 (2002).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.41.001236" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E41%2E001236&genre=article&stitle=Appl%2E%20Opt%2E&volume=41&issue=7&spage=1236&atitle=Compact%20beam%20expander%20with%20linear%20gratings">[360 Link]</a>
	
					</p>
				</div>
			
		</div>
		
		<div id="referenceByAuthor" class="sources-section page-section">
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor1">Aiki, K.</h4>
	    		<div id="referenceAuthor1" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor2">Akutsu, K.</h4>
	    		<div id="referenceAuthor2" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor3">Amitai, Y.</h4>
	    		<div id="referenceAuthor3" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-41-7-1236">R. Shechter,  Y. Amitai,  and  A. A. Friesem, “Compact beam expander with linear gratings,” Appl. Opt. 41, 1236–1240 (2002).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.41.001236" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E41%2E001236&genre=article&stitle=Appl%2E%20Opt%2E&volume=41&issue=7&spage=1236&atitle=Compact%20beam%20expander%20with%20linear%20gratings">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor4">Atac, R.</h4>
	    		<div id="referenceAuthor4" class="sources-container">
					<p class="source-block">
					R. Atac, “Applications of the scorpion color helmet-mounted cueing system,” Proc. SPIE 7688, 1–7 (2010). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.849287" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E849287&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=Applications%20of%20the%20scorpion%20color%20helmet%2Dmounted%20cueing%20system">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor5">Cameron, A.</h4>
	    		<div id="referenceAuthor5" class="sources-container">
					<p class="source-block">
					A. Cameron, “The application of holographic optical waveguide technology to Q-sight family of helmet mounted displays,” Proc. SPIE 7326, 1–11 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.818581" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E818581&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=The%20application%20of%20holographic%20optical%20waveguide%20technology%20to%20Q%2Dsight%20family%20of%20helmet%20mounted%20displays">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor6">Cheng, D.</h4>
	    		<div id="referenceAuthor6" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor7">Dereniak, E. L.</h4>
	    		<div id="referenceAuthor7" class="sources-container">
					<p class="source-block">
					E. L. Dereniak and  T. D. Dereniak, Geometrical and Trigonometric Optics (Cambridge University, 2008). 
	<br>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor8">Dereniak, T. D.</h4>
	    		<div id="referenceAuthor8" class="sources-container">
					<p class="source-block">
					E. L. Dereniak and  T. D. Dereniak, Geometrical and Trigonometric Optics (Cambridge University, 2008). 
	<br>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor9">Eisen, L.</h4>
	    		<div id="referenceAuthor9" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor10">Friesem, A. A.</h4>
	    		<div id="referenceAuthor10" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p><p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-41-7-1236">R. Shechter,  Y. Amitai,  and  A. A. Friesem, “Compact beam expander with linear gratings,” Appl. Opt. 41, 1236–1240 (2002).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.41.001236" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E41%2E001236&genre=article&stitle=Appl%2E%20Opt%2E&volume=41&issue=7&spage=1236&atitle=Compact%20beam%20expander%20with%20linear%20gratings">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor12">Golub, M.</h4>
	    		<div id="referenceAuthor12" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor13">Gurwich, I.</h4>
	    		<div id="referenceAuthor13" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor14">Jin, G.</h4>
	    		<div id="referenceAuthor14" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor15">Kress, B. C.</h4>
	    		<div id="referenceAuthor15" class="sources-container">
					<p class="source-block">
					B. C. Kress and  M. Shin, “Diffractive and holographic optics as optical combiners in head mounted displays,” presented at Ubicomp, Wearable Systems for Industrial Augmented Reality Applications, Zurich, Switzerland, 8–12 September2013. 
	<br>
	
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?&genre=article&stitle=presented%20at%20&atitle=Diffractive%20and%20holographic%20optics%20as%20optical%20combiners%20in%20head%20mounted%20displays">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor16">Kuwahara, M.</h4>
	    		<div id="referenceAuthor16" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor17">Matsumura, I.</h4>
	    		<div id="referenceAuthor17" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor18">Meyklyar, M.</h4>
	    		<div id="referenceAuthor18" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor19">Mukawa, H.</h4>
	    		<div id="referenceAuthor19" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor20">Nakano, S.</h4>
	    		<div id="referenceAuthor20" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor21">Shechter, R.</h4>
	    		<div id="referenceAuthor21" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-41-7-1236">R. Shechter,  Y. Amitai,  and  A. A. Friesem, “Compact beam expander with linear gratings,” Appl. Opt. 41, 1236–1240 (2002).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.41.001236" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E41%2E001236&genre=article&stitle=Appl%2E%20Opt%2E&volume=41&issue=7&spage=1236&atitle=Compact%20beam%20expander%20with%20linear%20gratings">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor22">Shin, M.</h4>
	    		<div id="referenceAuthor22" class="sources-container">
					<p class="source-block">
					B. C. Kress and  M. Shin, “Diffractive and holographic optics as optical combiners in head mounted displays,” presented at Ubicomp, Wearable Systems for Industrial Augmented Reality Applications, Zurich, Switzerland, 8–12 September2013. 
	<br>
	
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?&genre=article&stitle=presented%20at%20&atitle=Diffractive%20and%20holographic%20optics%20as%20optical%20combiners%20in%20head%20mounted%20displays">[360 Link]</a>
		
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor23">Song, W.</h4>
	    		<div id="referenceAuthor23" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor24">Wang, Y.</h4>
	    		<div id="referenceAuthor24" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor25">Weiss, V.</h4>
	    		<div id="referenceAuthor25" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor26">Welford, W. T.</h4>
	    		<div id="referenceAuthor26" class="sources-container">
					<p class="source-block">
					W. T. Welford, Aberrations of Optical Systems (Adam Hilger, 1986). 
	<br>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor27">Xu, C.</h4>
	    		<div id="referenceAuthor27" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
			    <h4 class='sub-header-toggle sub-list-header' data-action="slide-toggle" data-target="#referenceAuthor28">Yoshida, T.</h4>
	    		<div id="referenceAuthor28" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
		</div>
		
		<div id="referenceByPublication" class="sources-section page-section">
			
				<h4 class="sub-header-toggle sub-list-header" data-action="slide-toggle" data-target="#referencePublication1">Appl. Opt. (2)</h4>
				<div id="referencePublication1" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-45-17-4005">L. Eisen,  M. Meyklyar,  M. Golub,  A. A. Friesem,  I. Gurwich,  and  V. Weiss, “Planar configuration for image projection,” Appl. Opt. 45, 4005–4011 (2006).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.45.004005" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E45%2E004005&genre=article&stitle=Appl%2E%20Opt%2E&volume=45&issue=17&spage=4005&atitle=Planar%20configuration%20for%20image%20projection">[360 Link]</a>
	
					</p><p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=ao-41-7-1236">R. Shechter,  Y. Amitai,  and  A. A. Friesem, “Compact beam expander with linear gratings,” Appl. Opt. 41, 1236–1240 (2002).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/AO.41.001236" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FAO%2E41%2E001236&genre=article&stitle=Appl%2E%20Opt%2E&volume=41&issue=7&spage=1236&atitle=Compact%20beam%20expander%20with%20linear%20gratings">[360 Link]</a>
	
					</p>
				</div>
			
				<h4 class="sub-header-toggle sub-list-header" data-action="slide-toggle" data-target="#referencePublication3">J. Soc. Inf. Disp. (1)</h4>
				<div id="referencePublication3" class="sources-container">
					<p class="source-block">
					H. Mukawa,  K. Akutsu,  I. Matsumura,  S. Nakano,  T. Yoshida,  M. Kuwahara,  and  K. Aiki, “A full color eyewear display using planar waveguides with reflection volume holograms,” J. Soc. Inf. Disp. 17, 185–193 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1889/JSID17.3.185" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1889%2FJSID17%2E3%2E185&genre=article&stitle=J%2E%20Soc%2E%20Inf%2E%20Disp%2E%C2%A0&atitle=A%20full%20color%20eyewear%20display%20using%20planar%20waveguides%20with%20reflection%20volume%20holograms">[360 Link]</a>
		
					</p>
				</div>
			
				<h4 class="sub-header-toggle sub-list-header" data-action="slide-toggle" data-target="#referencePublication4">Opt. Express (1)</h4>
				<div id="referencePublication4" class="sources-container">
					<p class="source-block">
					
		<a href="http://www.osapublishing.org/abstract.cfm?&uri=oe-22-17-20705">D. Cheng,  Y. Wang,  C. Xu,  W. Song,  and  G. Jin, “Design of an ultra thin near display with geometrical waveguide and freeform optics,” Opt. Express 22, 20705–20719 (2014).</a>
	
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1364/OE.22.020705" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1364%2FOE%2E22%2E020705&genre=article&stitle=Appl%2E%20Opt%2E&volume=22&issue=17&spage=20705&atitle=Design%20of%20an%20ultra%2Dthin%20near%2Deye%20display%20with%20geometrical%20waveguide%20and%20freeform%20optics">[360 Link]</a>
	
					</p>
				</div>
			
				<h4 class="sub-header-toggle sub-list-header" data-action="slide-toggle" data-target="#referencePublication5">Proc. SPIE (2)</h4>
				<div id="referencePublication5" class="sources-container">
					<p class="source-block">
					A. Cameron, “The application of holographic optical waveguide technology to Q-sight family of helmet mounted displays,” Proc. SPIE 7326, 1–11 (2009). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.818581" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E818581&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=The%20application%20of%20holographic%20optical%20waveguide%20technology%20to%20Q%2Dsight%20family%20of%20helmet%20mounted%20displays">[360 Link]</a>
		
					</p><p class="source-block">
					R. Atac, “Applications of the scorpion color helmet-mounted cueing system,” Proc. SPIE 7688, 1–7 (2010). 
	<br>
	
			<a class="ref_crossref" href="https://doi.org/10.1117/12.849287" target="_blank">[Crossref] <span class="fa fa-external-link"></span></a>
		
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?id=doi:10%2E1117%2F12%2E849287&genre=article&stitle=Proc%2E%20SPIE%C2%A0&atitle=Applications%20of%20the%20scorpion%20color%20helmet%2Dmounted%20cueing%20system">[360 Link]</a>
		
					</p>
				</div>
			
				<h4 class="sub-header-toggle sub-list-header" data-action="slide-toggle" data-target="#referencePublication7">Other (3)</h4>
				<div id="referencePublication7" class="sources-container">
					<p class="source-block">
					W. T. Welford, Aberrations of Optical Systems (Adam Hilger, 1986). 
	<br>
	
					</p><p class="source-block">
					E. L. Dereniak and  T. D. Dereniak, Geometrical and Trigonometric Optics (Cambridge University, 2008). 
	<br>
	
					</p><p class="source-block">
					B. C. Kress and  M. Shin, “Diffractive and holographic optics as optical combiners in head mounted displays,” presented at Ubicomp, Wearable Systems for Industrial Augmented Reality Applications, Zurich, Switzerland, 8–12 September2013. 
	<br>
	
		<a class="ref_openurl" href="http://by5az4zr6x.search.serialssolutions.com/?L=BY5AZ4ZR6X&tab=ALL?&genre=article&stitle=presented%20at%20&atitle=Diffractive%20and%20holographic%20optics%20as%20optical%20combiners%20in%20head%20mounted%20displays">[360 Link]</a>
		
					</p>
				</div>
			
		</div>
		
					</div>
					
						<div id="articleCitations" class="article-section page-section small">
							<h3 class="article-heading">Cited By</h3>


	<p>OSA participates in <a href="http://www.crossref.org/">Crossref's Cited-By Linking service</a>. Citing articles from OSA journals and other participating publishers are listed here.</p>

	<p><a href="#" data-toggle="modal" data-target="#setCitationModal"><i class="icon-citation-alert"></i> Alert me when this article is cited.</a></p>
	<br>
	<div id="forwardlinks"><p><a class="view_citing" href="javascript:showFL('id=347995&qcr=1')">Click here to see a list of articles that cite this paper</a></p></div>
	
						</div>
						
						<div id="articleFigures" class="article-section page-section media-section ">
							<h3 class="article-heading">Figures (8)</h3>


	<div class="scroller-container">
		<div class="media-block thumbnail-scroller" id="figscroller">
			<ul class="slidee">
				
				<li class="slide">
					<a href="#figanchor1" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5tZWRpdW0sYW8tNTUtMjItNTkyNC1nMDAx" alt="Fig. 1."></div>
					<div class="thumbnail-label">Fig. 1.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor2" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=QC5tZWRpdW0sYW8tNTUtMjItNTkyNC1nMDAy" alt="Fig. 2."></div>
					<div class="thumbnail-label">Fig. 2.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor3" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=Lm1lZGl1bSxhby01NS0yMi01OTI0LWcwMDM" alt="Fig. 3."></div>
					<div class="thumbnail-label">Fig. 3.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor4" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=QC5tZWRpdW0sYW8tNTUtMjItNTkyNC1nMDA0" alt="Fig. 4."></div>
					<div class="thumbnail-label">Fig. 4.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor5" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=dTcqLm1lZGl1bSxhby01NS0yMi01OTI0LWcwMDU" alt="Fig. 5."></div>
					<div class="thumbnail-label">Fig. 5.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor6" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=QC5tZWRpdW0sYW8tNTUtMjItNTkyNC1nMDA2" alt="Fig. 6."></div>
					<div class="thumbnail-label">Fig. 6.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor7" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=QC5tZWRpdW0sYW8tNTUtMjItNTkyNC1nMDA3" alt="Fig. 7."></div>
					<div class="thumbnail-label">Fig. 7.</div>
					</a>
				</li>
				
				<li class="slide">
					<a href="#figanchor8" class="link-block">
					<div class="thumbnail"><img src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=Lm1lZGl1bSxhby01NS0yMi01OTI0LWcwMDg" alt="Fig. 8."></div>
					<div class="thumbnail-label">Fig. 8.</div>
					</a>
				</li>
				
			</ul>
		</div>
		<a href="#" class="scroll-arrow-left"><span class="icon-arrow-left"></span></a>
		<a href="#" class="scroll-arrow-right"><span class="icon-arrow-right"></span></a>
	</div>

	
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDE" name="figanchor1" id="figanchor1">
		      <img id="Figure[1]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDE" alt="Fig. 1.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 1.</strong> See-through head-mounted display employing a geometric wave
						guide.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g001&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g001&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g001&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=dTcqLmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwMg" name="figanchor2" id="figanchor2">
		      <img id="Figure[2]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=dTcqLmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwMg" alt="Fig. 2.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 2.</strong> (a) Planar waveguide configuration consisting of a waveguide
						substrate index <inline-formula><math display="inline" id="m12">
								<mrow>
									<msub>
										<mi>n</mi>
										<mi>d</mi>
									</msub>
								</mrow>
							</math></inline-formula> and coupling diffractive grating
						frequency <inline-formula><math display="inline" id="m13">
								<mrow>
									<mi>G</mi>
								</mrow>
							</math></inline-formula>. (b) Diffractive waveguide for
						which the <inline-formula><math display="inline" id="m14">
								<mrow>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mtext>fov</mtext>
										</mrow>
									</msub>
								</mrow>
							</math></inline-formula> is at a maximum.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g002&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g002&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g002&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDM" name="figanchor3" id="figanchor3">
		      <img id="Figure[3]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDM" alt="Fig. 3.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 3.</strong> Field of view of a planar geometric waveguide as a function of
								<inline-formula><math display="inline" id="m39">
								<mrow>
									<mi>G</mi>
									<mi>λ</mi>
								</mrow>
							</math></inline-formula>.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g003&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g003&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g003&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDQ" name="figanchor4" id="figanchor4">
		      <img id="Figure[4]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=OG0kcC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDQ" alt="Fig. 4.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 4.</strong> (a) Curved waveguide configuration consisting of two concentric
						surfaces with radii <inline-formula><math display="inline" id="m44">
								<mrow>
									<mi>R</mi>
								</mrow>
							</math></inline-formula> and <inline-formula><math display="inline" id="m45">
								<mrow>
									<mi>R</mi>
									<mo>+</mo>
									<mi>t</mi>
								</mrow>
							</math></inline-formula>, waveguide substrate index
								<inline-formula><math display="inline" id="m46">
								<mrow>
									<msub>
										<mi>n</mi>
										<mi>d</mi>
									</msub>
								</mrow>
							</math></inline-formula> and coupling diffractive grating
						frequency <inline-formula><math display="inline" id="m47">
								<mrow>
									<mi>G</mi>
								</mrow>
							</math></inline-formula>. (b) Diffractive waveguide for
						which the <inline-formula><math display="inline" id="m48">
								<mrow>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mtext>fov</mtext>
										</mrow>
									</msub>
								</mrow>
							</math></inline-formula> is at a maximum. Setting
								<inline-formula><math display="inline" id="m49">
								<mrow>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mi>f</mi>
										</mrow>
									</msub>
									<mo>=</mo>
									<mi>U</mi>
								</mrow>
							</math></inline-formula> allows an opening QU ray trace,
						described in Appendix <span class="ref"><nolink data-action="change-section" data-target="articleBody" href="#ref1" onclick="highlightRef('ref1')">A</a></span>, to
						be used to find angles <inline-formula><math display="inline" id="m50">
								<mrow>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mi>d</mi>
										</mrow>
									</msub>
								</mrow>
							</math></inline-formula> and <inline-formula><math display="inline" id="m51">
								<mrow>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mi>i</mi>
										</mrow>
									</msub>
								</mrow>
							</math></inline-formula> across surface
								<inline-formula><math display="inline" id="m52">
								<mrow>
									<mi>R</mi>
								</mrow>
							</math></inline-formula>.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g004&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g004&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g004&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=dTcqLmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwNQ" name="figanchor5" id="figanchor5">
		      <img id="Figure[5]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=dTcqLmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwNQ" alt="Fig. 5.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 5.</strong> Maximum FOV for a curved waveguide as a function of quantity
								<inline-formula><math display="inline" id="m86">
								<mrow>
									<mi>d</mi>
									<mo>/</mo>
									<mi>R</mi>
								</mrow>
							</math></inline-formula> for a various substrate indices
						of refraction <inline-formula><math display="inline" id="m87">
								<mrow>
									<msub>
										<mi>n</mi>
										<mi>d</mi>
									</msub>
								</mrow>
							</math></inline-formula>.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g005&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g005&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g005&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=QC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDY" name="figanchor6" id="figanchor6">
		      <img id="Figure[6]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=QC5sYXJnZSxhby01NS0yMi01OTI0LWcwMDY" alt="Fig. 6.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 6.</strong> Geometry of a microprism structure applied to a geometric waveguide
						substrate index <inline-formula><math display="inline" id="m89">
								<mrow>
									<msub>
										<mi>n</mi>
										<mi>d</mi>
									</msub>
								</mrow>
							</math></inline-formula>. Microprism structure has wedge
						angle <inline-formula><math display="inline" id="m90">
								<mrow>
									<mi>α</mi>
								</mrow>
							</math></inline-formula>. Ray angles for incident marginal
						ray <inline-formula><math display="inline" id="m91">
								<mrow>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mi>FOV</mi>
										</mrow>
									</msub>
								</mrow>
							</math></inline-formula> and the refracted ray angle
								<inline-formula><math display="inline" id="m92">
								<mrow>
									<msub>
										<mi>θ</mi>
										<mi>d</mi>
									</msub>
								</mrow>
							</math></inline-formula> inside the waveguide.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g006&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g006&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g006&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=LmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwNw" name="figanchor7" id="figanchor7">
		      <img id="Figure[7]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=LmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwNw" alt="Fig. 7.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 7.</strong> Schematic for QU ray trace for a real ray. +/− signs are
							used to denote sign convention of the angles. The top figure
							shows the ray trace across an interface. The bottom figure
							shows the opening of a ray trace where our axial ray is
							traced. The opening ray trace is directly applicable to
							solving for <inline-formula><math display="inline" id="m97">
									<mrow>
										<msub>
											<mrow>
											<mi>θ</mi>
											</mrow>
											<mrow>
											<mi>i</mi>
											</mrow>
										</msub>
									</mrow>
								</math></inline-formula> in Fig. <span class="ref"><nolink data-action="change-section" data-target="articleBody" href="#g004">4</a></span>.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g007&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g007&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g007&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
		<div class="media-block">
		  <figure class="media">
		    <a href="http://imagebank.osa.org/getImage.xqy?img=dTcqLmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwOA" name="figanchor8" id="figanchor8">
		      <img id="Figure[8]" class="media-image" src="/images/ajax-loader-big.gif" data-src="http://imagebank.osa.org/getImage.xqy?img=dTcqLmxhcmdlLGFvLTU1LTIyLTU5MjQtZzAwOA" alt="Fig. 8.">
		      <span class="fi-arrows-out media-popout"></span>
		    </a>
		  </figure>
		  <div class="media-description">
		    <strong>Fig. 8.</strong> Ray propagation in curved waveguide composed of two
								concentric spheres.
		  </div>
		  <div class="media-actions">
		    <p class="small"><a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g008&imagetype=full" target="_blank">Download Full Size</a>  |  <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g008&imagetype=pwr" target="_blank">PPT Slide</a> | <a href="/viewmedia.cfm?uri=ao-55-22-5924&figure=ao-55-22-5924-g008&imagetype=pdf" target="_blank">PDF</a>
		  </div>
		</div>
		
						</div>
						
						<div id="articleEquations" class="article-section page-section small">
							
<h3 class="article-heading">Equations (36)</h3>


	<p class="small">Equations on this page are rendered with MathJax. <a href="http://www.mathjax.org/help/user/" target="_blank">Learn more.</a></p>
	
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (1)</a>
		<math display="block" label="(1)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>c</mi>
								</mrow>
							</msub>
							<mo>≤</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>≤</mo>
							<mfrac>
								<mrow>
									<mi>π</mi>
								</mrow>
								<mrow>
									<mn>2</mn>
								</mrow>
							</mfrac>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (2)</a>
		<math display="block" label="(2)">
						<mrow>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<msub>
								<mrow>
									<mo>−</mo>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (3)</a>
		<math display="block" label="(3)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mrow>
								<mo>(</mo>
								<mfrac>
									<mrow>
										<msub>
											<mrow>
											<mi>n</mi>
											</mrow>
											<mrow>
											<mi>d</mi>
											</mrow>
										</msub>
										<mtext> </mtext>
										<mi>sin</mi>
										<mtext> </mtext>
										<msub>
											<mrow>
											<mi>θ</mi>
											</mrow>
											<mrow>
											<mi>d</mi>
											</mrow>
										</msub>
										<mo>−</mo>
										<mi>G</mi>
										<mi>λ</mi>
									</mrow>
									<mrow>
										<msub>
											<mrow>
											<mi>n</mi>
											</mrow>
											<mrow>
											<mi>i</mi>
											</mrow>
										</msub>
									</mrow>
								</mfrac>
								<mo>)</mo>
							</mrow>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (4)</a>
		<math display="block" label="(4)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
									<mtext> </mtext>
									<mi>max</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (5)</a>
		<math display="block" label="(5)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>c</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mrow>
								<mo>(</mo>
								<mfrac>
									<mrow>
										<mn>1</mn>
									</mrow>
									<mrow>
										<msub>
											<mrow>
											<mi>n</mi>
											</mrow>
											<mrow>
											<mi>d</mi>
											</mrow>
										</msub>
									</mrow>
								</mfrac>
								<mo>)</mo>
							</mrow>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (6)</a>
		<math display="block" label="(6)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
									<mtext> </mtext>
									<mi>min</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<mn>1</mn>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (7)</a>
		<math display="block" label="(7)">
						<mrow>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<mn>1</mn>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>≤</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
								</mrow>
							</msub>
							<mo>≤</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (8)</a>
		<math display="block" label="(8)">
						<mrow>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>≥</mo>
							<mn>0</mn>
							<mspace linebreak="newline"/>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<mn>1</mn>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>≤</mo>
							<mn>0</mn>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (9)</a>
		<math display="block" label="(9)">
						<mrow>
							<mfrac>
								<mrow>
									<mn>1</mn>
								</mrow>
								<mrow>
									<mi>λ</mi>
								</mrow>
							</mfrac>
							<mo>≤</mo>
							<mi>G</mi>
							<mo>≤</mo>
							<mfrac>
								<mrow>
									<msub>
										<mrow>
											<mi>n</mi>
										</mrow>
										<mrow>
											<mi>d</mi>
										</mrow>
									</msub>
								</mrow>
								<mrow>
									<mi>λ</mi>
								</mrow>
							</mfrac>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (10)</a>
		<math display="block" label="(10)">
						<mrow>
							<mi>G</mi>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<msub>
										<mrow>
											<mi>n</mi>
										</mrow>
										<mrow>
											<mi>d</mi>
										</mrow>
									</msub>
									<mo>+</mo>
									<mn>1</mn>
								</mrow>
								<mrow>
									<mn>2</mn>
									<mi>λ</mi>
								</mrow>
							</mfrac>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (11)</a>
		<math display="block" label="(11)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>−</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mo stretchy="false">(</mo>
							<mn>1</mn>
							<mo>−</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo stretchy="false">)</mo>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (12)</a>
		<math display="block" label="(12)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mrow>
								<mo>(</mo>
								<mn>1</mn>
								<mo>+</mo>
								<mfrac>
									<mrow>
										<mi>d</mi>
									</mrow>
									<mrow>
										<mi>R</mi>
									</mrow>
								</mfrac>
								<mo>)</mo>
							</mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
								</mrow>
							</msub>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (13)</a>
		<math display="block" label="(13)">
						<mrow>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mrow>
								<mo>(</mo>
								<mn>1</mn>
								<mo>+</mo>
								<mfrac>
									<mrow>
										<mi>d</mi>
									</mrow>
									<mrow>
										<mi>R</mi>
									</mrow>
								</mfrac>
								<mo>)</mo>
							</mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
								</mrow>
							</msub>
							<mo>+</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (14)</a>
		<math display="block" label="(14)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<msub>
										<mrow>
											<mi>n</mi>
										</mrow>
										<mrow>
											<mi>d</mi>
										</mrow>
									</msub>
									<mtext> </mtext>
									<mi>sin</mi>
									<mtext> </mtext>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mi>d</mi>
										</mrow>
									</msub>
									<mo>−</mo>
									<mi>G</mi>
									<mi>λ</mi>
								</mrow>
								<mrow>
									<mn>1</mn>
									<mo>+</mo>
									<mfrac>
										<mrow>
											<mi>d</mi>
										</mrow>
										<mrow>
											<mi>R</mi>
										</mrow>
									</mfrac>
								</mrow>
							</mfrac>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (15)</a>
		<math display="block" label="(15)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
									<mtext> </mtext>
									<mi>max</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<msub>
										<mrow>
											<mi>n</mi>
										</mrow>
										<mrow>
											<mi>d</mi>
										</mrow>
									</msub>
									<mo>−</mo>
									<mi>G</mi>
									<mi>λ</mi>
								</mrow>
								<mrow>
									<mn>1</mn>
									<mo>+</mo>
									<mfrac>
										<mrow>
											<mi>d</mi>
										</mrow>
										<mrow>
											<mi>R</mi>
										</mrow>
									</mfrac>
								</mrow>
							</mfrac>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (16)</a>
		<math display="block" label="(16)">
						<mrow>
							<mi>U</mi>
							<mo>=</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mo>+</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (17)</a>
		<math display="block" label="(17)">
						<mrow>
							<mi>Q</mi>
							<mo>=</mo>
							<mi>R</mi>
							<mo stretchy="false">(</mo>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mi>sin</mi>
							<mtext> </mtext>
							<mi>U</mi>
							<mo stretchy="false">)</mo>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (18)</a>
		<math display="block" label="(18)">
						<mrow>
							<msub>
								<mrow>
									<mi>Q</mi>
								</mrow>
								<mrow>
									<mn>2</mn>
								</mrow>
							</msub>
							<mo>=</mo>
							<mi>Q</mi>
							<mo>+</mo>
							<mi>t</mi>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<mi>U</mi>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (19)</a>
		<math display="block" label="(19)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>c</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<mi>R</mi>
								</mrow>
								<mrow>
									<mi>R</mi>
									<mo>+</mo>
									<mi>t</mi>
								</mrow>
							</mfrac>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mfrac>
								<mrow>
									<mi>R</mi>
									<mo>−</mo>
									<mi>t</mi>
								</mrow>
								<mrow>
									<mi>R</mi>
									<mo>+</mo>
									<mi>t</mi>
								</mrow>
							</mfrac>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<mi>U</mi>
							<mo>+</mo>
							<mi>sin</mi>
							<mtext> </mtext>
							<mi>U</mi>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (20)</a>
		<math display="block" label="(20)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>c</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<mi>R</mi>
								</mrow>
								<mrow>
									<mi>R</mi>
									<mo>+</mo>
									<mi>t</mi>
								</mrow>
							</mfrac>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<mrow>
								<mo>[</mo>
								<mfrac>
									<mrow>
										<mi>R</mi>
										<mo>−</mo>
										<mi>t</mi>
									</mrow>
									<mrow>
										<mi>R</mi>
										<mo>+</mo>
										<mi>t</mi>
									</mrow>
								</mfrac>
								<mo>−</mo>
								<mn>1</mn>
								<mo>]</mo>
							</mrow>
							<mo>⁢</mo>
							<mi>sin</mi>
							<mrow>
								<mo>(</mo>
								<msub>
									<mrow>
										<mi>θ</mi>
									</mrow>
									<mrow>
										<mi>f</mi>
									</mrow>
								</msub>
								<mo>+</mo>
								<msub>
									<mrow>
										<mi>θ</mi>
									</mrow>
									<mrow>
										<mi>d</mi>
									</mrow>
								</msub>
								<mo>−</mo>
								<msup>
									<mrow>
										<mi>sin</mi>
									</mrow>
									<mrow>
										<mo>−</mo>
										<mn>1</mn>
									</mrow>
								</msup>
								<mrow>
									<mo>(</mo>
									<mrow>
										<mo>(</mo>
										<mn>1</mn>
										<mo>+</mo>
										<mfrac>
											<mrow>
											<mi>d</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											</mrow>
										</mfrac>
										<mo>)</mo>
									</mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<msub>
										<mrow>
											<mi>θ</mi>
										</mrow>
										<mrow>
											<mi>f</mi>
										</mrow>
									</msub>
									<mo>)</mo>
								</mrow>
								<mo>)</mo>
							</mrow>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (21)</a>
		<math display="block" label="(21)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>c</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (22)</a>
		<math display="block" label="(22)">
						<mrow>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>f</mi>
									<mtext> </mtext>
									<mi>min</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<mfrac>
								<mrow>
									<mn>1</mn>
									<mo>−</mo>
									<mi>G</mi>
									<mi>λ</mi>
								</mrow>
								<mrow>
									<mn>1</mn>
									<mo>+</mo>
									<mfrac>
										<mrow>
											<mi>d</mi>
										</mrow>
										<mrow>
											<mi>R</mi>
										</mrow>
									</mfrac>
								</mrow>
							</mfrac>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (23)</a>
		<math display="block" label="(23)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mrow>
								<mo>(</mo>
								<mfrac>
									<mrow>
										<msub>
											<mi>n</mi>
											<mi>d</mi>
										</msub>
										<mo>−</mo>
										<mi>G</mi>
										<mi>λ</mi>
									</mrow>
									<mrow>
										<mn>1</mn>
										<mo>+</mo>
										<mfrac>
											<mi>d</mi>
											<mi>R</mi>
										</mfrac>
									</mrow>
								</mfrac>
								<mo>)</mo>
							</mrow>
							<mo>−</mo>
							<msup>
								<mrow>
									<mi>sin</mi>
								</mrow>
								<mrow>
									<mo>−</mo>
									<mn>1</mn>
								</mrow>
							</msup>
							<mrow>
								<mo>(</mo>
								<mfrac>
									<mrow>
										<mn>1</mn>
										<mo>−</mo>
										<mi>G</mi>
										<mi>λ</mi>
									</mrow>
									<mrow>
										<mn>1</mn>
										<mo>+</mo>
										<mfrac>
											<mrow>
											<mi>d</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											</mrow>
										</mfrac>
									</mrow>
								</mfrac>
								<mo>)</mo>
							</mrow>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (24)</a>
		<math display="block" label="(24)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>+</mo>
							<mi>α</mi>
							<mo>,</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (25)</a>
		<math display="block" label="(25)">
						<mrow>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>o</mi>
								</mrow>
							</msub>
							<mo>=</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>+</mo>
							<mi>α</mi>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (26)</a>
		<math display="block" label="(26)">
						<mrow>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>+</mo>
							<mi>α</mi>
							<mo stretchy="false">)</mo>
							<mo>=</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>+</mo>
							<mi>α</mi>
							<mo stretchy="false">)</mo>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (27)</a>
		<math display="block" label="(27)">
						<mrow>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>cos</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>cos</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo stretchy="false">)</mo>
							<mi>tan</mi>
							<mtext> </mtext>
							<mi>α</mi>
							<mo>=</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>sin</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (28)</a>
		<math display="block" label="(28)">
						<mrow>
							<mo stretchy="false">(</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>i</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>cos</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>FOV</mi>
								</mrow>
							</msub>
							<mo>−</mo>
							<msub>
								<mrow>
									<mi>n</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mtext> </mtext>
							<mi>cos</mi>
							<mtext> </mtext>
							<msub>
								<mrow>
									<mi>θ</mi>
								</mrow>
								<mrow>
									<mi>d</mi>
								</mrow>
							</msub>
							<mo stretchy="false">)</mo>
							<mi>tan</mi>
							<mtext> </mtext>
							<mi>α</mi>
							<mo stretchy="false">↔</mo>
							<mi>G</mi>
							<mi>λ</mi>
							<mo>.</mo>
						</mrow>
					</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A1)</a>
		<math display="block" label="(A1)">
							<mrow>
								<mi>sin</mi>
								<mtext> </mtext>
								<mi>I</mi>
								<mo>=</mo>
								<mfrac>
									<mrow>
										<mi>Q</mi>
									</mrow>
									<mrow>
										<mi>R</mi>
									</mrow>
								</mfrac>
								<mo>+</mo>
								<mi>sin</mi>
								<mtext> </mtext>
								<mi>U</mi>
								<mo>.</mo>
							</mrow>
						</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A2)</a>
		<math display="block" label="(A2)">
							<mrow>
								<msup>
									<mrow>
										<mi>n</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mtext> </mtext>
								<mi>sin</mi>
								<mtext> </mtext>
								<msup>
									<mrow>
										<mi>I</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>=</mo>
								<mi>n</mi>
								<mtext> </mtext>
								<mi>sin</mi>
								<mtext> </mtext>
								<mi>I</mi>
								<mo>.</mo>
							</mrow>
						</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A3)</a>
		<math display="block" label="(A3)">
							<mrow>
								<msup>
									<mrow>
										<mi>U</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>−</mo>
								<msup>
									<mrow>
										<mi>I</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>=</mo>
								<mi>U</mi>
								<mo>−</mo>
								<mi>I</mi>
								<mo>.</mo>
							</mrow>
						</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A4)</a>
		<math display="block" label="(A4)">
							<mrow>
								<msup>
									<mrow>
										<mi>Q</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>=</mo>
								<mi>R</mi>
								<mo stretchy="false">(</mo>
								<mi>sin</mi>
								<mtext> </mtext>
								<msup>
									<mrow>
										<mi>I</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>−</mo>
								<mi>sin</mi>
								<mtext> </mtext>
								<msup>
									<mrow>
										<mi>U</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo stretchy="false">)</mo>
								<mo>.</mo>
							</mrow>
						</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A5)</a>
		<math display="block" label="(A5)">
							<mrow>
								<msub>
									<mrow>
										<mi>Q</mi>
									</mrow>
									<mrow>
										<mn>2</mn>
									</mrow>
								</msub>
								<mo>=</mo>
								<msup>
									<mrow>
										<mi>Q</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>+</mo>
								<mi>t</mi>
								<mtext> </mtext>
								<mi>sin</mi>
								<mtext> </mtext>
								<msup>
									<mrow>
										<mi>U</mi>
									</mrow>
									<mrow>
										<mo>′</mo>
									</mrow>
								</msup>
								<mo>.</mo>
							</mrow>
						</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A6)</a>
		<math display="block" label="(A6)">
								<mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<mi>I</mi>
									<mo>=</mo>
									<mrow>
										<mo>(</mo>
										<mfrac>
											<mrow>
											<mo>−</mo>
											<mi>t</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											<mo>+</mo>
											<mi>t</mi>
											</mrow>
										</mfrac>
										<mo>+</mo>
										<mn>1</mn>
										<mo>)</mo>
									</mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<mi>U</mi>
									<mo>.</mo>
								</mrow>
							</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A7)</a>
		<math display="block" label="(A7)">
								<mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<msub>
										<mrow>
											<mi>I</mi>
										</mrow>
										<mrow>
											<mn>2</mn>
										</mrow>
									</msub>
									<mo>=</mo>
									<mrow>
										<mo>(</mo>
										<mfrac>
											<mrow>
											<mi>t</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											</mrow>
										</mfrac>
										<mo>+</mo>
										<mn>1</mn>
										<mo>)</mo>
									</mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<mi>I</mi>
									<mo>.</mo>
								</mrow>
							</math>
		</div>
		
		<div class="article-math-block">
		  <a class="math-controls hidden-xs hidden-sm" data-target="#mathJaxHelp"><span class="icon-options"></span> (A8)</a>
		<math display="block" label="(A8)">
								<mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<msub>
										<mrow>
											<mi>I</mi>
										</mrow>
										<mrow>
											<mn>2</mn>
										</mrow>
									</msub>
									<mo>=</mo>
									<mrow>
										<mo>(</mo>
										<mfrac>
											<mrow>
											<mi>t</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											</mrow>
										</mfrac>
										<mo>+</mo>
										<mn>1</mn>
										<mo>)</mo>
									</mrow>
									<mrow>
										<mo>(</mo>
										<mfrac>
											<mrow>
											<mo>−</mo>
											<mi>t</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											<mo>+</mo>
											<mi>t</mi>
											</mrow>
										</mfrac>
										<mo>+</mo>
										<mn>1</mn>
										<mo>)</mo>
									</mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<mi>U</mi>
									<mo>=</mo>
									<mrow>
										<mo>(</mo>
										<mfrac>
											<mrow>
											<mi>t</mi>
											<mo>+</mo>
											<mi>R</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											</mrow>
										</mfrac>
										<mo>)</mo>
									</mrow>
									<mrow>
										<mo>(</mo>
										<mfrac>
											<mrow>
											<mo>−</mo>
											<mi>t</mi>
											<mo>+</mo>
											<mi>R</mi>
											<mo>+</mo>
											<mi>t</mi>
											</mrow>
											<mrow>
											<mi>R</mi>
											<mo>+</mo>
											<mi>t</mi>
											</mrow>
										</mfrac>
										<mo>)</mo>
									</mrow>
									<mi>sin</mi>
									<mtext> </mtext>
									<mi>U</mi>
									<mo>=</mo>
									<mi>sin</mi>
									<mtext> </mtext>
									<mi>U</mi>
									<mo>.</mo>
								</mrow>
							</math>
		</div>
		
						</div>
						
						<div id="articleMetrics" class="article-section page-section">
							<h3 class="article-heading">Metrics</h3>


	<script type="text/Javascript">
	var metricsloaded = false;
	function showMetrics () {
		if (metricsloaded == false) {
			$('#metricsiframe').attr('src','https://cm.scholarlyiq.com/ArticleCompleteView.htm?publisher=OSA&article=347995&username=osacharts&hashedPassword=wexvlUOkPxSJncOLbve5rAjB5nY%3D&style=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fcss%2Fmetrics%2Ecss');
			metricsloaded = true;
		}
	}
	</script>

	<iframe id="metricsiframe" width="100%"/></iframe>

						</div>
						
				</div>
				
			</div>
		</div>
	</div>
	

	<div class="hidden-sm hidden-md hidden-lg secondary-navigation--footer">
		
<div class="secondary-nav-container">
	<nav class="secondary-nav">
		<ul class="secondary-nav-list">
			<li class="secondary-nav-item hidden-md hidden-lg hidden-sm">
				<div class="mobile-journal-header">
		<a href="/ao/home.cfm"><h1>Applied Optics</h1>
		<p class="primary-editors">Ronald Driggers, Editor-in-Chief</p></a>
	</div>
			</li>
			<li class="secondary-nav-item hidden-xs ">
				<a href="/ao/home.cfm" class="secondary-nav-item_link">Journal Home</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/journal/ao/about.cfm">About</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/upcomingissue.cfm">Issues in Progress</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/issue.cfm">Current Issue</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/browse.cfm">All Issues</a>
			</li>
			<li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/upcoming.cfm">Early Posting</a>
			</li> <li class="secondary-nav-item ">
				<a class="secondary-nav-item_link" href="/ao/feature.cfm">Feature Issues</a>
			</li>
		</ul>
	</nav>
</div>

	</div>
</div>



<script type="text/Javascript">
function alertconfirmed() {
	// Send ajax to confirm
	document.getElementById('confirmAlertButton').disabled = 'disabled';
	$.ajax({
		cache: false,
		type: "GET",
		url: "set_citation_alert.cfm?uri=ao-55-22-5924&confirmalert=true"
	}).done(function( ajxretmsg ) {
		if (ajxretmsg == "1") {
			document.getElementById('alertConfirmArea').innerHTML = '<strong>Your alert has been set!</strong><br><br><input type="button" name="closeEmail" data-dismiss="modal" value="Close">';
		} else {
			document.getElementById('alertConfirmArea').innerHTML = 'There was an error setting your alert.  Please try again later.<br><br><input type="button" name="closeEmail" data-dismiss="modal" value="Close">';
		}
	});
}
</script>

<div class="modal fade" id="setCitationModal" tabindex="-1" role="dialog" aria-labelledby="setCitationLabel" aria-hidden="true">
	<div class="modal-dialog login-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="setCitationLabel">Confirm Citation Alert</h4>
			</div>
			<div class="modal-body">
				
					Please <a data-toggle="modal" data-target="#userLogin">login</a> to set citation alerts.
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="mathJaxHelp" tabindex="-1" role="dialog" aria-labelledby="mathJaxHelp" aria-hidden="true">
	<div class="modal-dialog login-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				MathJax Help
			</div>
			<div class="modal-body">
				Equations displayed with <a href="http://www.mathjax.org" target="_blank">MathJax</a>. Right click equation to reveal menu options.
			</div>
		</div>
	</div>
</div> 
<div class="container ft-top">
	<div class="pull-left">
		<ul class="nav navbar-nav navbar-ft">
			<li class="dropdown ft-nav-left"><a href="/">Home</a></li>
			<li class="dropdown ft-nav-right"><a href="#">To Top <span class="fi-arrow-up"></span></a></li>
			
				<li class="dropdown ft-nav-left">
					<a href="abstract.cfm?uri=ao-55-22-5917" class="ft-article-prev"><span class="icon-arrow-left"></span> Previous Article</a>
				</li>
			
				<li class="dropdown ft-nav-right">
					<a href="abstract.cfm?uri=ao-55-22-5931" class="ft-article-next">Next Article <span class="icon-arrow-right"></span></a>
				</li>
			
		</ul>
	</div>
	<div class="pull-right">
		<ul class="nav navbar-nav navbar-right navbar-ft">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Favorites <strong class="caret"></strong></a>
				<ul class="dropdown-menu up">
			          
          <li><a href="/user">Go to My Account</a></li>
          
          <li><a href="#" data-toggle="modal" data-target="#userLogin">Login to access favorites</a></li>
          
				</ul>
			</li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Recent Pages <strong class="caret"></strong></a>
				<ul class="dropdown-menu up recent-page-list">
				</ul>
			</li>
		</ul>
	</div>
</div>


<div id="formErrors" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button class="close" data-dismiss="modal" aria-label="close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title">Field Error</h4>
			</div>
			<div class="modal-body">
				<p id="formErrorsText"></p>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal">Ok</button>
			</div>
		</div>
	</div>
</div>

<div id="avsModal" class="avs-modal">
  <div id="avsModalContent" class="avs-modal-content">
    <strong>Select <span id="titleFiltersPopup"></span> as filters</strong>
    
    <hr>
    <div class="row">
      <div class="col-md-8">
        <div class="input-collapse">
          <input id="input-popup-selectize" type="text" class="form-control selectized" placeholder="(type names or look up list)" tabindex="-1"/>
          <div class="selectize-control form-control multi"></div>
        </div>
      </div>
    </div>
    <hr>
    <ul id="checkBoxList"></ul>
    <hr>
    <span id="avsModalSubmit" class="btn btn-primary">Select Topics</span> <span id="avsModalClose" class="cancel"> Cancel</span>
  </div>
</div>
<div class="container ft-links"><a id="global-nav"></a>
  <div class="col-sm-3">
    <ul class="ft-links-list">
    <li><a href="/about.cfm">Journals</a></li>
    <li><a href="/conferences.cfm">Proceedings</a></li>
    <li class="bullet hidden-xs"><a href="/conferences.cfm">By Year</a></li>
    <li class="bullet hidden-xs"><a href="/conferences.cfm?findby=conference">By Name</a></li>
    <li class="ft-links-header hidden-xs">Regional Sites</li>
    <li class="hidden-xs"><a href="/china/">OSA Publishing China</a></li>
  </ul>
  </div>

 <div class="col-sm-3">
  <ul class="ft-links-list">
    <li class="ft-links-header">Information for</li>
    <li><a href="/author/author.cfm">Authors</a></li>
    <li><a href="/submit/review/peer_review.cfm">Reviewers</a></li>
    <li><a href="/library/">Librarians</a></li>
  </ul>
  <ul class="ft-links-list">
    <li class="ft-links-header">Open Access Information</li>
    <li><a href="/submit/review/open-access-policy-statement.cfm">Open Access Statement and Policy</a></li>
    <li><a href="/library/license_v1.cfm">Terms for Journal Article Reuse</a></li>
  </ul>
  </div>

  <div class="col-sm-3">
    <ul class="ft-links-list">
    <li class="ft-links-header">Other Resources</li>
    <li><a href="/books/default.cfm">OSAP Bookshelf</a></li>
    <li><a href="/oida/reports.cfm">OIDA Reports</a></li>
    <li><a href="http://www.osa-opn.org" target="_blank">Optics &amp; Photonics News <span class="fa fa-external-link"></span></a></li>
    <li><a href="http://imagebank.osa.org" target="_blank">Optics ImageBank <span class="fa fa-external-link"></span></a></li>
    <li><a href="/spotlight/">Spotlight on Optics</a></li>
  </ul>
  <!-- mobile only regional sites link -->
  <ul class="ft-links-list hidden-sm hidden-md hidden-lg">
      <li class="ft-links-header">Regional Sites</li>
      <li><a href="/china/">OSA Publishing China</a></li>
  </ul>
  </div>

  <div class="col-sm-3">
    <ul class="ft-links-list">
    <li class="ft-links-header">About</li>
    <li><a href="/about.cfm">About OSA Publishing</a></li>
    <li><a href="/benefitslog.cfm">About My Account</a></li>
    <li><a href="/contactus.cfm">Contact Us</a></li>
	<li><a href="#" onclick="popUpWindow('/submitFeedback.cfm?url='+escape(document.location.href),700,490); return false;">Send Us Feedback</a></li>
	</ul>
  </div>
  
  <div class="row">
    <div class="col-xs-12 col-md-3 col-md-offset-9 text-right">
      <a class="ft-links-header" href="http://www.twitter.com/osapublishing" target="_blank" aria-label="Follow @OsaPublishing on Twitter">
        <span class="fa fa-2x fa-twitter-square">&nbsp;</span>
      </a>
      <a class="ft-links-header" href="http://www.facebook.com/opticalsociety" target="_blank" aria-label="Follow us on Facebook">
        <span class="fa fa-2x fa-facebook-square">&nbsp;</span>
      </a>
      <a class="ft-links-header" href="https://www.linkedin.com/groups/56568/profile" target="_blank" aria-label="Follow us on LinkedIn">
        <span class="fa fa-2x fa-linkedin-square">&nbsp;</span>
      </a>
    </div>
  </div>
  </div>

<div class="ft-bottom">
  <div class="container">
    <div class="col-sm-6">
      &copy; Copyright 2019 | The Optical Society. All Rights Reserved
    </div>
    <div class="col-sm-6 ft-right">
      <a href="/privacy.cfm">Privacy</a> | <a href="/termsofuse.cfm">Terms of Use</a>
    </div>
  </div>
</div> 
<div class="modal fade" id="userLogin">
  <div class="modal-dialog login-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Login or <a href="https://account.osa.org/eweb/dynamicpage.aspx?sso=1&site=osac&webcode=loginrequired&url_success=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fao%2Fabstract%2Ecfm%3Frwjcode%3Dao%26uri%3Dao%2D55%2D22%2D5924%26usertoken%3D%7Btoken%7D">Create Account</a></h4>
      </div>
      <div class="modal-body">

        <form class="form-horizontal" role="form">

          <div class="alert alert-danger loginErr"></div>
          <div class="alert alert-warning loginLoading">Please wait...</div>

          <div class="form-group">
            <label for="inputEmail" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="inputEmail" placeholder="Email" autofocus>
            </div>
          </div>
          <div class="form-group">
            <label for="inputPassword" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword" placeholder="Password">
              <span class="help-block"><a href="https://account.osa.org/eweb/Dynamicpage.aspx?webcode=forgotpassword&Site=osac">Forgot your password?</a></span>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <div class="checkbox">
                <label>
                  <input type="checkbox" id="rememberme"> Remember me on this computer
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-primary" id="loginButton">Login</button>
              <button type="submit" class="btn btn-default" data-dismiss="modal" data-target="#userLogin">Cancel</button>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <div class="col-xs-12" style="text-align: left"><a href="https://auth.osa.org/connect/authorize?client_id=osap&response_type=id_token token&scope=openid email profile affiliation&redirect_uri=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fdefault%2Ecfm&state=C541DDF8-38E7-495F-91832346DAE88463|https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fabstract%2Ecfm%3Frwjcode%3Dao%26uri%3Dao%2D55%2D22%2D5924&nonce=C541DDF8-38E7-495F-91832346DAE88463&acr_values=idp:oa&response_mode=form_post">OpenAthens Login</a></div>
      </div>
      <div class="modal-footer">
        <div class="col-xs-12" style="text-align: left"><a href="https://auth.osa.org/connect/authorize?client_id=osap&response_type=id_token token&scope=openid email profile affiliation&redirect_uri=https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fdefault%2Ecfm&state=C541DDF8-38E7-495F-91832346DAE88463|https%3A%2F%2Fwww%2Eosapublishing%2Eorg%2Fabstract%2Ecfm%3Frwjcode%3Dao%26uri%3Dao%2D55%2D22%2D5924&nonce=C541DDF8-38E7-495F-91832346DAE88463&acr_values=idp:cstnet&response_mode=form_post">CAoS Institutions (China) Login</a></div>
      </div>
      <div class="modal-footer">
        <div class="col-xs-6" style="text-align: left"><a href="/privacy.cfm">OSA Privacy Policy</a></div>
        <div class="col-xs-6"><a href="http://www.osa.org/en-us/help/">Need help?</a> <span class="fi-info"></span></div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --> 
	</div>

<script type="text/x-mathjax-config">
  MathJax.Hub.Config({
    MathML: {
      // fixing some spacing issues by forcing it to use the MathML spacing instead of TeX
      useMathMLspacing: true
    },
    // workaround for MathZoom overflow issues.
    MathZoom: {
      styles: {
        "#MathJax_Zoom": {
          overflow: "auto !important"
        }
      }
    },
    CommonHTML: {
      mtextFontInherit: true,
      linebreaks: {
        automatic: true
      }
    },
    displayAlign: "center",
    "HTML-CSS": {
      linebreaks: {
        automatic: true
      },
      availableFonts: ["Gyre-Pagella"],
      preferredFont: "Gyre-Pagella",
      webFont: "Gyre-Pagella",
      mtextFontInherit: true,
      scale: 85,
      noReflows: false
    },

	  SVG: {
	  	linebreaks: {
	  		automatic: true
	  	} ,
	  	font: "Gyre-Pagella",
	  	mtextFontInherit: true
	  },

    menuSettings: {
      zoom: "Click",
      zscale: "200%"
    },

	  //testing MatchWebFonts extension
	  MatchWebFonts: {
	  	matchFor: {
	  		"HTML-CSS": true,
	  		NativeMML: true,
	  		SVG: true
	  	}
	  },
    tex2jax: {inlineMath: [["$","$"],["\\(","\\)"]], processEscapes: true},
    extensions: ["toMathML.js"]
  });
  // end intitial config
  MathJax.Hub.Register.StartupHook("End Jax", function() {
    var browser = MathJax.Hub.Browser;
    var versionNumberStart = browser.version.substring(0, browser.version.indexOf("."));
    if (browser.isSafari && (versionNumberStart === "5")) {
      console.log("Safari 5.x detected, rerendering in CommonHTML");
      jax = "CommonHTML";
      MathJax.Hub.setRenderer(jax);
    } 
  });
  MathJax.Hub.Register.StartupHook("MathML Jax Ready",function () {
    var MATHML = MathJax.InputJax.MathML,
      MML = MathJax.ElementJax.mml;
    var MAKEMML = MATHML.Parse.prototype.MakeMML;
    MATHML.Parse.Augment({
      MakeMML: function (node) {
        //
        //  Convert to MathJax internal format
        //
        var mml = MAKEMML.call(this,node);
        //
        //  If it is an <mi> with text ending in a combining character
        //    create an <mover> element with the original <mi> text
        //    and an <mo> containing the combining character.
        //
        if (mml.isa(MML.mi)) {
          var text = mml.data.join("");
          if (text.length > 1) {
            var c = text.charCodeAt(text.length-1);
            if ((c >= 0x300 && c < 0x360) || (c >= 0x20D0 && c < 0x2100)) {
              mml = MML.mover(
                MML.mi(text.substr(0,text.length-1)),
                MML.mo(text.charAt(text.length-1))
              );
            }
          }
        }
        return mml;
      }
    });
  });
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

<!-- <script>
document.write('<script src=' +
('__proto__' in {} ? 'js/vendor/zepto' : 'js/vendor/jquery') +
'.js><\/script>')
</script> -->

<script src="/js/vendor/jquery-1.10.2.min.js"></script>
<script src="/js/vendor/jquery.autocomplete.min.js"></script>
<script src="/js/vendor/selectize.js"></script>
<script src="/js/vendor/selectize.custom.js"></script>
<script src="/js/vendor/sly.min.js"></script>
<script src="/js/jquery.tinysort.min.js"></script>
<script src="/js/app.js?v=10162018"></script>
<script src="/js/recentPages.js?v=01012018"></script>

<script src="/js/vendor/bootstrap-concat.min.js"></script>
<script src="/js/quickpager.jquery.js"></script>
<script src="/js/jquery.unveil.js"></script>

<script type="text/javascript" src="/js/jquery.tokeninput.js"></script>
<script type="text/javascript" src="/js/jquery.base64.js"></script>
<script src="/js/search.js?v=01012018"></script>

<script type="text/javascript" src="/js/underscore.js"></script>
<script type="text/javascript" src="/js/jquery-ui-1.10.2.custom.min.js"></script>

<script type="text/javascript" src="/js/search/ApiClient.js?v=10292018a"></script>
<script type="text/javascript" src="/js/search/Bus.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Config.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Constants.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Template.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Facet.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Pagination.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Results.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Search.js?v=02222018"></script>
<script type="text/javascript" src="/js/search/Topics.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Util.js?v=01012018"></script>
<script type="text/javascript" src="/js/search/Main.js?v=01012018"></script>
<!-- templates -->
<script type="text/template" id="template-page-size-selector">
    <%= selected %>
    <ul class="dropdown-menu">
        <%= unselected %>
    </ul>
</script>
<script type="text/template" id="template-pagination-selected-page-size">
    <a class="page_size_<%= size %> btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
      <%= size %>
      <span class="caret"></span>
    </a>
</script>
<script type="text/template" id="template-pagination-unselected-page-size">
    <li><a class="page_size_<%= size %>" href="#"><%= size %></a></li>
</script>

<script type="text/template" id="template-pagination-unselected-page-number">
    <li><a href="#" data-value="<%= number %>"><%= label %></a></li>
</script>
<script type="text/template" id="template-pagination-disabled-unselected-page-number">
    
</script>
<script type="text/template" id="template-pagination-selected-page-number">
    <li class="active"><a href="#" data-value="<%= number %>"><%= label %></a></li>
</script>

<script type="text/template" id="search-result-row">
    <li class="sr-item">
        <input type="checkbox" class="sri-select hidden-xs" name="articles" value="<%= documentUri %>"/>
        <div class="sri-summary">
            <h3 class="sri-title"><a href="<%= path %>abstract.cfm?uri=<%= documentUri %>&origin=search" target="_blank"><%= title %></a></h3>
            <span class="sri-authors"><%= authors %></span>
            <ul class="sri-details">
                <li class="sri-year"><%= year %></li>
                <li class="sri-journal"><i class="<%= access%>"></i> <%= name %><strong>&nbsp; View: <%= htmlLinks %><%= pdfLinks %></strong><%= xinfo %></li>
                
            </ul>
            <div class="sri-toggle-extended fi-plus">
            </div>
        </div>

        <div class="sri-extended">
            <p class="sri-abstract"><%= abstract %></p>

            <strong class="sri-hits-heading">
                Search Hits
            </strong>
            <ul class="sri-hits list-unstyled">
                <%= searchHits %>
            </ul>
            <img src="<%= imgLink%>" class="sri-image" height="50" width="50" alt="" onerror="this.parentNode.removeChild(this);" />
        </div>
    </li>
</script>

<script type="text/template" id="search-result-hit">
    <li>&hellip;<%= hitText %>&hellip;</li>
</script>

<script type="text/template" id="search-result-snippet-highlighted"><strong><%= text %></strong></script>

<script type="text/template" id="search-result-snippet-xinfo"> [<%= xinfo %>]</script>

<script type="text/template" id="search-result-snippet-pdf"><a href="<%= pdfUri %>" target="_blank">PDF</a></script>

<script type="text/template" id="search-result-snippet-html"><a href="<%= htmlUri %>" target="_blank">HTML</a> | </script>

<script type="text/template" id="search-result-element-visible">display:inline</script>

<script type="text/template" id="search-result-element-hidden">display:none</script>

<script type="text/template" id="search-result-element-nbsp">&nbsp;</script>

<script type="text/template" id="search-result-facet">
    <h5 class="bg1">
        <span class="float_l">
            <input type="checkbox" name="chkAllFacet-<%= name %>">
                <%= facetName %>
            </input>
        </span>
        <%= moreLink %>
        <a id="sortby-<%= name %>" class="sidebar_more white" href="#">Sort</a>
    </h5>
    <ul id="facet-values-<%= name %>">
        <%= facetValues %>
    </ul>
</script>

<script type="text/template" id="facet-publications">
    <ul class="sf-list">
        <li class="sf-list_list-item"><input id="publicationsAll" class="sf-select-all sf-list_input" type="checkbox" checked="checked" disabled="disabled"/> <label class="sf-list_label" for="publicationsAll">All</label></li>
        <li class="sf-list_list-item"><input id="facet-publications-journals" class="sf-list_input" type="checkbox" name="publications"/> <label class="sf-list_label" for="facet-publications-journals">Journals <span class="facet-count">(<%= totalJournals %>)</span></label></li>
        <li class="sf-list_list-item"><input id="facet-publications-proceedings" class="sf-list_input" type="checkbox" name="proceedings"/> <label class="sf-list_label" for="facet-publications-proceedings">Proceedings <span class="facet-count">(<%= totalProceedings %>)</span></label></li>
    </ul>
	</div>
	<div class="sf-module sf-journals">
		<div class="row2">
    	<h3 id="facet-journal-show" class="sf-group-title collapsed">Journals</h3>
		<div id="sortJournal" class="sf-sort"></div>
		</div>
            <div id="facet-journal-values">
            </div>  
	</div>
	<div class="sf-module sf-proceedings">
		<div class="row2">
    	<h3 id="facet-proceeding-show" class="sf-group-title collapsed">Proceedings</h3>
			<div id="sortProceeding" class="sf-sort"></div>
		</div>
		<div  id="facet-proceeding-values"></div>
	</div>
</script>

<script type="text/template" id="search-result-facet-basics">

    <ul class="sf-list">
		<li class="sf-select-all"><input id="<%= allId %>" type="checkbox" checked="checked" disabled="disabled"/> All</li>
		<%= values %>	
		<li class="sf-expanded">
			<a href="#" id="more-<%= allId %>" class="search-panel_more-link" data-toggle="modal" data-target="#modal-<%= allId %>">more <%=name%> &raquo;</a>
			<div class="modal full fade" id="modal-<%= allId %>">
				<div class="modal-dialog">
					<div class="modal-content search">
						<div class="row mbottom-20">
							<div class="col-md-4">
								<h3 class="modal-header search"><%= modalTitle %></h3>
							</div>
							<div class="col-md-6 col-md-offset-2">						
								<input id="input-<%= allId%>" type="text" class="form-control selectized" placeholder="(type names or look up list)" tabindex="-1"></input>
								<div class="selectize-control form-control multi"></div>                 					
							</div>			
						</div>
						<div class="modal_option-container mbottom-20 scrollable" id="moreUl-<%=allId%>">
						</div>
						
						<hr>
						<a href="#" id="apply-<%= allId %>"  class="btn btn-primary" data-dismiss="modal">Apply Filters</a>
						<a href="#" id="cancel-<%= allId %>" class="btn btn-default" data-dismiss="modal">Cancel</a>
					</div>
				</div>

			</div>
        </li>		
	</ul>  	
	
</script>

<script type="text/template" id="search-result-facet-terms">

    <ul class="sf-list">
		<li class="sf-select-all">
			<input id="<%= allId %>" type="checkbox" checked="checked" disabled="disabled"/>
			<label for="<%= allId %>">Remove all topic filters</label>
		</li>
		<%= values %>	
		<li class="sf-expanded">
			<a href="#" id="more-<%= allId %>" class="search-panel_more-link hidden-xs" data-toggle="modal" data-target="#modal-<%= allId %>">include more topics &raquo;</a>
			<div class="modal full fade" id="modal-<%= allId %>">
				<div class="modal-dialog">
					<div class="modal-content search">
						<div class="row mbottom-20">
							<div class="col-md-4">
								<h3 class="modal-header search">Optics &amp; Photonics Topics</h3>
							</div>
							<div class="col-md-6 col-md-offset-2">
								<input id="input-<%= allId%>" type="text" class="form-control" placeholder="Search for a specific topic" tabindex="-1"></input>
							</div>			
						</div>
						<div class="row mbottom-20">
							<div class="col-md-12">
								<strong>Browse the topics:</strong> 
								Click the <svg class="selection-list_arrow"><use xlink:href="#right-arrow" /></svg> to reveal subtopics. Use the checkbox to select a topic to filter your search.
							</div>
						</div>

						<div class="modal_option-container mbottom-20 scrollable" id="moreUl-<%=allId%>">
						</div>

						<div class="row">
							<div class="col-md-12">
								<strong>Selected Topics</strong>
							</div>
						</div>

						<div id="moreSelectedList"></div>
						<div class="row"><div class="col-md-12">&nbsp;<!-- spacing row --></div></div>

						<a href="#" id="apply-<%= allId %>" class="btn btn-primary" data-dismiss="modal">Add Selected Topic Filters</a>
						<a href="#" id="cancel-<%= allId %>" class="btn btn-default" data-dismiss="modal">Cancel</a>
					</div>
				</div>
			</div>
        </li>		
	</ul>  	
	
</script>

<script type="text/template" id="search-result-facet-journal-proceedings">
<ul class="sf-list">
	<li class="sf-select-all"><label><input id="<%= allId %>" type="checkbox" checked> All</label></li>
	<%= values %>
	<li class="sf-expanded">
		<a href="#" id="more-<%= allId %>" class="search-panel_more-link" data-toggle="modal" data-target="#modal-<%= allId %>" class="sf-expanded-toggle">more <%=name%> &raquo;</a>
		<div class="modal full fade" id="modal-<%= allId %>">
			<div class="modal-dialog">
				<div class="modal-content search">
					<div class="row mbottom-20">
						<div class="col-md-4">
							<h3 class="modal-header search"><%= modalTitle%></h3>
						</div>
						<div class="col-md-6 col-md-offset-2">
							<input id="input-<%= allId%>" type="text" class="form-control selectized" placeholder="(type names or look up list)" tabindex="-1"></input>
							<div class="selectize-control form-control multi"></div>                  
							
						</div>
					</div>
					<div class="modal_option-container mbottom-20 scrollable" id="moreUl-<%=allId%>">
					</div>
						
						
					<hr>
					<a href="#" id="apply-<%= allId %>"  class="btn btn-primary" data-dismiss="modal">Apply Filters</a>
					<a href="#" id="cancel-<%= allId %>" class="btn btn-default" data-dismiss="modal">Cancel</a>
				</div>
			</div>
		</div>
    </li>
 </ul>
</script>

<script type="text/template" id="search-result-facet-content">
	
	<ul  class="sf-list">
		<%= values %>	
	</ul>
</script>

<script type="text/template" id="facet-more-new-content">
<div class="more-columns_container">
	<div class="more-columns_column"><ul class="selection-list"><%= val1%></ul></div>
	<div class="more-columns_column"><ul class="selection-list"><%= val2%></ul></div>
	<div class="more-columns_column"><ul class="selection-list"><%= val3%></ul></div>
</div>
</script>

<script type="text/template" id="facet-more-terms-div">

<div class="more-columns_container">
	<div class="more-columns_column" id="<%= name%>-col1"><%= val%></div>
	<div class="more-columns_column" id="<%= name%>-col2"></div>
	<div class="more-columns_column" id="<%= name%>-col3"></div>
</div>

</script>

<script type="text/template" id="facet-more-terms-list">
	<ul class="selection-list"><%= val%></ul>
</script>

<script type="text/template" id="search-result-facet-more">
    <a id="more-<%= name %>" href="#" class="sidebar_more white">More</a>
</script>

<script type="text/template" id="search-result-facet-detail">
    <a href="#" id="details-link-<%= id %>" class="sidebar_more black">details</a>
    <ul id="details-container-<%= id %>" class="list-unstyled" style="display:none;" data-value="<%= valName %>"></ul>
</script>

<script type="text/template" id="search-result-facet-value">
    <li class="sf-list_list-item <%= rowclass %>" style="<%= display %>">
        <input type="checkbox" class="sf-list_input <%= boxstyle %>" name="chkFacet-<%= name %>" value="<%= code %>" data-value="<%= dataValue %>" id="<%= id %>" <%= checked %> acronym="<%= acronym %>" />
	<% if (hasCloseButton === true) { %>
		<div <% if (isbold != '') { %> class="sf-list_label sf-authors-bold" <% } else { %> class="sf-list_label" style="margin-bottom:5px"<% } %>><%= valName %><span class="facet-count"><%= valCount %></span> <span class="fa-times-circle-o fa facet-value_toggle"></span></div> <%= detail%> 
	<% } else { %>
		<label <%= title%> for="<%= id %>"  <% if (isbold != '') { %> class="sf-list_label sf-authors-bold" <% } else { %> class="sf-list_label" <% } %>><%= valName %> <span class="facet-count"><%= valCount %></span></label> <%= detail%> 
	<% } %>
    </li>
</script>

<script type="text/template" id="more-term-facet-value">
    <li class="selection-list_list-item" style="<%= display %>">
        <input class="selection-list_checkbox" type="checkbox" name="<%= name %>" data-value="<%= dataValue %>" <%= checked %> <%= boxstyle %>/><div class="selection-list_link" name="text-<%= name %>" data-value="<%= dataValue %>" <%= clazz %>><%= valName %><% if (valCount != "") { %> <span class="more-facet-count"><%= valCount %></span><% } %></div><% if (isx === true) { %><a href="#" class="selection-list_expander <%= inputname%>-expandin"><svg class="selection-list_arrow"><use xlink:href="#right-arrow" /></svg></a><% } %>
    </li>
</script>

<script type="text/template" id="more-term-selected">
    <div class="selected-facet" data-topic="<%= finalTopic %>">
        <%= val %>
        <a href="#" name="selected-x" data-value="<%= dataValue %>" count="<%= cnt %>" class="selected-facet_close" ><span class="fa-times-circle-o fa"></span></a>
    </div>
</script>

<script type="text/template" id="search-result-facet-class">
    class=""
</script>
<script type="text/template" id="search-result-facet-style-normal">
    style="font-weight:normal"
</script>

<script type="text/template" id="search-result-facet-sort-popup">
    <p>
        <input type="radio" id='<%= name %>:item-order:ascending' name="facetSortBy" value='<%= name %>:item-order:ascending' >
        <label for='<%= name %>:item-order:ascending'><%= label %> ascending</label><br>
        <input type="radio" id='<%= name %>:item-order:descending' name="facetSortBy" value='<%= name %>:item-order:descending' >
        <label for='<%= name %>:item-order:descending'><%= label %> descending</label><br>
        <input type="radio" id='<%= name %>:frequency-order:ascending' name="facetSortBy" value='<%= name %>:frequency-order:ascending' >
        <label for='<%= name %>:frequency-order:ascending'>Frequency ascending</label><br>
        <input type="radio" id='<%= name %>:frequency-order:descending' name="facetSortBy" value='<%= name %>:frequency-order:descending'>
        <label for='<%= name %>:frequency-order:descending'>Frequency descending</label>
    </p>
</script>

<script type="text/template" id="filter-selected">
    <li class="label filter-tag filter-tag--<%= filterType %>"> <%= filterValue %> <a class="inline-dismiss"><span name="filterSelected" id="<%= filterId %>" class="fi-x" data-value="<%= filterValue %>"></span></a></li>
</script>
  
  <script type="text/template" id="filter-search">
    <li class="filter-search label filter-tag filter-tag--<%= filterType %>"> <%= filterLabel %> <a class="inline-dismiss"><span name="filterSelected" id="<%= filterId %>" class="fi-x" data-value="<%= filterValue %>"></span></a></li>
</script>

<script type="text/template" id="publication-sort-template">
		<button class="sf-sort-toggle"><span class="icon-sort"></span><span class="sr-only">sort</span></button>
		<div class="sf-sort-popup">
			<ul>
				<li class="active"><a href="#" name="sort-<%= type%>" data-value="<%= type%>:frequency-order:descending">Article Count</a></li>
				<li><a href="#" name="sort-<%= type%>" data-value="<%= type%>:item-order:ascending">Alphabetical A>Z</a></li>
				<li><a href="#" name="sort-<%= type%>" data-value="<%= type%>:item-order:descending">Alphabetical Z>A</a></li>
			</ul>
		</div>

</script>
<script type="text/template" id="author-topic-sort-template">

		<button class="sf-sort-toggle"><span class="icon-sort"></span><span class="sr-only">sort</span></button>
		<div class="sf-sort-popup">
			<ul>
				<li class="active"><a href="#" name="sort-<%= type%>" data-value="<%= type%>:frequency-order:descending">Article Count</a></li>
				<li><a href="#" name="sort-<%= type%>" data-value="<%= type%>:item-order:ascending">Alphabetical A>Z</a></li>
				<li><a href="#" name="sort-<%= type%>" data-value="<%= type%>:item-order:descending">Alphabetical Z>A</a></li>
			</ul>
		</div>

</script>
<script type="text/template" id="year-sort-template">

		<button class="sf-sort-toggle"><span class="icon-sort"></span><span class="sr-only">sort</span></button>
		<div class="sf-sort-popup">
			<ul>
				<li class="active"><a href="#" name="sort-year" data-value="year:item-order:descending">Newest date first</a></li>
				<li><a href="#" name="sort-year" data-value="year:item-order:ascending">Oldest date first</a></li>
				<li><a href="#" name="sort-year" data-value="year:frequency-order:descending">Article Count</a></li>
			</ul>
		</div>

</script>
<script type="text/template" id="more-pop-up-template">
		<%= values%>
</script>
<script type="text/template" id="loading-gif-template">
	<li><img style="position:center; display:none;" src="../images/ajax-loader.gif"/></li>
</script>
<script type="text/template" id="all-plus-separator-template">
    <li class="sf-select-all"><label><input id="<%= allId %>" type="checkbox" > All</label></li>
</script>
<script type="text/template" id="more-topic-column-header">
	<li class="selection-list_header"><span class="fa fa-caret-right"></span><%= name %></li>
</script>

<script type="text/template" id="loader">
	<div class="progress">
		<div class="progress-bar progress-bar-striped active" role="progress-bar" id="progress-<%= allId %>"></div>
	</div>
</script> 

<script type="text/javascript">     
	$(document).ready(function() {
		
	showFL('id=347995&qcr=1');
	
	if (location.hash.indexOf("figanchor") != -1) {
		$("#figurestablink").click();
		$(location.hash).get(0).scrollIntoView();
	}
$.get('/checkjs.cfm?s=E4C75SS');
		populateRecentPages();
		$("img").removeClass("lazyimg").unveil(100);
	});
</script>
<script type="text/javascript">

 var gaAcct = "UA-1200622-15";

 function downloadJSAtOnload() {
 var element = document.createElement("script");
 element.src = "/js/loadJS.js";
 document.body.appendChild(element);
 }
 if (window.addEventListener)
 window.addEventListener("load", downloadJSAtOnload, false);
 else if (window.attachEvent)
 window.attachEvent("onload", downloadJSAtOnload);
 else window.onload = downloadJSAtOnload;
 </script>


<!-- web2 -->
</body>
</html> 