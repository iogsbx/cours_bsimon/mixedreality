import numpy as np
import cv2
cap = cv2.VideoCapture(1)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # SIFT
    surf = cv2.xfeatures2d.SURF_create(5000)
    kp, des = surf.detectAndCompute(gray, None)
    frame=cv2.drawKeypoints(gray,kp,None,(255,0,0),4)
    cv2.imshow('surf',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
