import numpy as np
import cv2
cap = cv2.VideoCapture(1)
fast = cv2.FastFeatureDetector_create()
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # FAST
    kp= fast.detect(gray,None)
    frame=cv2.drawKeypoints(gray, kp , None, color = (255, 0, 0))
    cv2.imshow('fast',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
